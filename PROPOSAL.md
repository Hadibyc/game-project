# PROPOSITION DE JEU - Groupe 1
 
# Werewolf Hunter
 
## Présentation rapide
 
Jeu d’action/stratégie conçu pour deux joueurs sur un même écran (un viewport par joueur). 
 
## Affichage du jeu
 
- Les deux viewports seront séparés par une bande verticale.
- On pourra placer des bandes d’informations horizontales en haut et en bas (framerate, tick, barres de vie, temps restant, pouvoirs disponibles …)
 
## L’univers du jeu
 
### Les règles
 
- Un joueur est chasseur : il possède une arme de corps-à-corps. Son objectif est d’aller directement frapper l’autre joueur. Pour cela, il faudra que le traqué se trouve sur la case devant lui. Si les tests révèlent que cette situation est compliquée à atteindre durant une course poursuite, on pourra augmenter la portée à 2 cases.
- L’autre joueur doit se cacher : il peut incarner presque toutes les entités présentes (buissons, arbres, lapins… mais pas les zombies et l’autre joueur) en se positionnant devant l’entité et en effectuant une action précise.
- Un rythme jour/nuit permettra d’inverser les rôles en cours de jeu et ainsi de renverser la partie. A la tombée de la nuit, le traqué se transforme en loup-garou et traque le chasseur, le chasseur devient donc le traqué et les zombies se mettent du côté du loup-garou. Au matin, les rôles sont à nouveau inversés. De plus, les zombies de jour sont légèrement plus rapides alors que les zombies de nuit ont un pouvoir de détection légèrement plus puissant.
 
 
#### Objectif   
- le chasseur : tuer la personne qui se cache.
- le traqué : survivre jusqu’à l’inversion des rôles.
- Si personne ne meurt à la fin du temps réglementaire, il y a égalité.
 
#### Début du jeu
- Le traqué apparaît vers le milieu du monde, sur une case définie.
- Le chasseur apparaîtra sur la case à côté mais ne pourra pas bouger.
- Pendant x secondes, l’écran du chasseur est noir. Le traqué peut donc chercher une cachette.
- C’est seulement après ce temps de cachette que le décompte de la journée commence.
 
#### Jeu
- Le chasseur devant toucher le traqué, la vitesse de déplacement du chasseur sera légèrement plus élevée. La vitesse des personnages pourra être régulée avec la vitesse de transition/animation d’une case à l’autre.
- Le chasseur sera aidé par des zombies présents sur la carte. Ils changent de camp et d’apparence en fonction du rythme jour/nuit. On devra donc changer l’avatar qui représente l’entité ainsi que l’adversaire visé. Cet avatar aura également des propriétés différentes : on pourra moduler le rayon de détection et la vitesse tout en veillant à conserver l’équilibre du jeu.
- Ces zombies détectent automatiquement le traqué à partir d’un certain rayon (et redevient normal si le joueur sort d’un rayon plus grand que celui de détection). Ils se dirigent alors vers lui et lui infligent quelques dégâts au contact. Leur objectif est de donner une petite aide au chasseur et d’éviter que le traqué soit immobile. On implémente alors la fonction Closest() des automates pour que la recherche d'ennemis s’effectue uniquement dans ce rayon.
- Le traqué peut prendre possession des entités du décor. Pour prendre possession d’une entité, cette dernière doit être sur la case devant lui. Il doit donc pouvoir se tourner sans forcément bouger. Il faut donc veiller à ce que le premier appui sur une touche directionnelle oriente simplement le joueur. A chaque fois que le traqué prend possession d’une entité, cette dernière a une animation permettant de signaler qu’il en a bien pris possession(avec wizz du traqué). Une fois que le joueur veut revenir à son apparence initiale, il doit réappuyer sur la même touche et l’objet laché reste donc à cette position. Si une entité se trouve à la case en face de celle dont on sort, on prend directement possession de cette nouvelle entité. Dans le cas où l’entité en face ne peut pas être possédée, alors on ne peut pas sortir.
 
#### Modélisation du monde
- Notre vue sera représentée par un système de grille, avec des cases assez grosses (juste un peu plus petites que la taille d’un personnage).
- Le modèle du monde sera donc représenté dans un tableau à deux dimensions finies et défini, avec une génération aléatoire des positions entités (décoratives / non joueurs et non zombies) au début de la partie.
- Ce sera donc un monde qui se replie sur lui-même : lorsqu’on atteint le bord droit du tableau, on revient sur le bord gauche. De même pour la dimension verticale.
- Pour représenter cela au niveau du Viewport, on affichera les cases du tableau de coordonnées (x modulo width, y modulo height) où : (x,y) sont les coordonnées du personnage dans le jeu et (width,height) sont les dimensions du tableau. 
 
#### Modélisation des entités dans le monde
- Une entité possède des coordonnées pour la vue : par convention, le coin haut-gauche de la case la plus haute et la plus à gauche occupée visuellement par l’entité.
- L’entité possède également une largeur et une hauteur.
- L’entité aura également des coordonnées pour le modèle : la case qu’elle occupe dans la matrice du modèle. C’est avec ces coordonnées qu’on déterminera les cases occupées.
 
#### Les déplacements dans le modèle
- Les entités avancent case par case dans le modèle avec un système de réservation. On implémente au niveau de la vue des animations pour donner l’impression d’un déplacement fluide.
- Exemple pour le système de réservation : dans la matrice de notre modèle, si un personnage se déplace vers le haut, la case du haut est réservée dans la matrice. Elle est considérée comme occupée, mais pas par l’entité elle-même. L’animation de transition de case commence alors. Lorsque l’animation arrive à la moitié de sa progression, le changement d’état est véritablement effectué dans la matrice, la case sur laquelle on était est libérée et la case du haut est occupée par notre personnage.
- Lorsque la commande pour avancer est relâchée et que notre personnage est en cours de déplacement vers une autre case, il finit son déplacement.
- Les entrées claviers ne seront pas prises en compte durant une animation entre deux cases : lorsqu’on avance et qu’on demande de tourner à droite, notre personnage devra avoir fini l’animation pour avancer avant de commencer à aller à droite. Si l’appui sur la commande pour tourner à droite est trop brève, elle pourra donc ne pas être prise en compte (pas de système de file d’attente) : booléen pour savoir si l’entité est en cours d’animation ou pas.
 
#### Transitions jour/nuit
 
- Gérées par des timers. Par exemple une manche (jour ou nuit) pourrait durer 5 minutes (le temps qu’il reste avant chaque changement est affiché à l’écran)
- Au niveau du modèle, la nuit tombe brutalement sur l’ensemble de la carte.
- Au niveau de la vue, on pourra faire des animations pour assombrir/éclaircir le monde au fur et à mesure que le timer défile. Par exemple 30 secondes avant la fin de la manche un effet de dégradé jour/nuit serait appliqué.
 
#### Originalité      
- Le comportement de toute entité sera défini par des automates (écrit au format .gal). Ces automates pourront être interchangés par l’utilisateur dans le menu de configuration pour personnaliser le jeu.
- Les entités de décors seront également définies par des automates. Certaines ne feront rien (buissons), d’autres se déplaceront de manière aléatoire (lapins).
-  Lorsque le joueur traqué prend possession de l’entité, l’entité décor est détruite, et l’avatar du joueur prend la forme du décor. Lorsque le joueur quitte le décor, un décor est recréé (fonction EGG de l’automate) à cet endroit et l’avatar du joueur revient à sa forme originale.
- Si le traqué contrôle un élément de décor, sa vitesse en sera impactée (plus ou moins négativement en fonction de l’entité).
- Le chasseur pourra installer un nombre fixe de pièges. Ces pièges disparaîtront lorsqu’il change de rôle et qu’il devient le traqué. Ces pièges ne tueront pas le traqué mais le pénaliseront fortement.
- L’équilibre du jeu pourra être modulé avec l’apparition aléatoire de pouvoirs (un certain nombre par manches, à n’importe quel moment). Ces pouvoirs apparaîtront à l’écran sous forme d’un signal (entre les deux viewports). La nature du pouvoir n’est pas précisée sur ce signal. Les deux joueurs devront alors appuyer sur une touche spécifique à chacun d’eux le plus rapidement possible. Le premier qui appuie sur la touche récupère le pouvoir et il est révélé et stocké jusqu’à utilisation. Un joueur ne peut stocker qu’un pouvoir. Si un autre pouvoir apparaît, il peut appuyer sur la touche mais gardera son ancien pouvoir stocké (cela pénalisera juste l’autre joueur). Pour l’instant, nous n’avons prévu d’implémenter qu’un seul pouvoir : le pouvoir de soin, qui permettra à celui qui l’utilise de récupérer une partie de ses points de vie.
 
#### Options
 
Autres idées de pouvoirs :
 
Au fur et à mesure de l’implémentation, et si nous estimons que nous avons le temps de le faire, nous rajouterons un ou plusieurs pouvoirs supplémentaires comme par exemple : 
 
Un pouvoir de Dash, qui permettra d'avancer de x cases d’un seul coup. 
Un pouvoir permettant d’augmenter la puissance du chasseur (si c’est le chasseur qui l’utilise) ou de la diminuer (si c’est le traqué qui l’utilise).
Un pouvoir donnant un malus ou un bonus aux zombies en fonction de qui le prend.
Un pouvoir de camouflage, qui permettrait au joueur de se camoufler en zombie (ou de carrément disparaître) pendant une poignée de secondes.
Un pouvoir d’invincibilité, qui rendrait le joueur invulnérable, permettant au chasseur de frapper tout ce qui l’entoure sans recevoir de pénalité, et au traqué d’être invulnérable aux assauts de ses poursuivants. 
Un pouvoir permettant d’augmenter la durée de la manche (si le chasseur l’utilise) et de la diminuer (si le traqué l’utilise).
 
Pour un meilleur confort visuel, nous pourrions ajouter des petites flèches sur le sol indiquant l’orientation du joueur dans les cas où celui-ci a pris possession d’une entité, ces flèches seraient évidemment visibles uniquement par le joueur traqué.
 
#### Définir les actions Pop et Wizz
##### pour le traqué
+ Pop : Récupérer pouvoir apparu
+ Wizz: prendre/quitter possession de l’entité devant lui
 
##### pour le chercheur
+ Pop : Récupérer pouvoir apparu
+ Wizz: poser/ramasser piège sur la case actuelle
 
##### bot (zombie)
+ Pop : passage apparence nuit
+ Wizz: passage apparence jour
 
##### pour les éléments fixes (cailloux, buissons)
+  Pop : Clignote en rouge
+  Wizz: Clignote en jaune
 
##### pour les lapins
+  Pop : Clignote en rouge
+  Wizz: Clignote en jaune
 
##### pour les pièges
+  Pop : bloque 
+  Wizz: relâche 
 
 
## Le menu de reconfiguration
 
  Le jeu aura un menu de reconfiguration
  - il permet d'attribuer un automate, des caractéristiques à chaque entité.
  - il peut également modifier certains paramètres de génération de terrain (densité des décors, par type)
  - Ajuster la densité et les caractéristiques de zombies
  - Modifier la durée du jour et de la nuit ainsi que le nombre de cycles.
  - Modifier la durée de cachette en début de partie
 
## OPTIONS
Il peut être utile de donner l'orientation de l’entité possédé pour savoir sur quelle case on va “sortir” de l’entité. 
 
## Automates de chaque entité
 
### Automate pour le joueur 1
```
J1(Init){
  *(Init) :
    | Key(FU) & not(MyDir(N)) ? Turn(N) : (Init)
    | Key(FU) & Cell(N,V) ? Move(N) : (Init)
    | Key(FD) & not(MyDir(S)) ? Turn(S) : (Init)
    | Key(FD) & Cell(S,V) ? Move(S) : (Init)
    | Key(FL) & not(MyDir(O)) ? Turn(O) : (Init)
    | Key(FL) & Cell(O,V) ? Move(O) : (Init)
    | Key(FR) & not(MyDir(E)) ? Turn(E) : (Init)
    | Key(FR) & Cell(E,V) ? Move(E) : (Init)
    | Key(ENTER) ? Hit(F,A)  : (Init)
    | Key(l) ? Power : (Init)
    | Key(m) ? Wizz : (Init)
    | Key(p) ? Pop : (Init)
}
```

### Automate pour le joueur 2
```
J2(Init){
  *(Init) :
    | Key(z) & not(MyDir(N)) ? Turn(N) : (Init)
    | Key(z) & Cell(N,V) ? Move(N) : (Init)
    | Key(q) & not(MyDir(O)) ? Turn(O) : (Init)
    | Key(q) & Cell(O,V) ? Move(O) : (Init)
    | Key(s) & not(MyDir(S)) ? Turn(S) : (Init)
    | Key(s) & Cell(S,V) ? Move(S) : (Init)
    | Key(d) & not(MyDir(E)) ? Turn(E) : (Init)
    | Key(d) & Cell(E,V) ? Move(E) : (Init)
    | Key(SPACE) ? Hit(F,A)  : (Init)
    | Key(a)  ? Power : (Init)
    | Key(e)  ? Wizz : (Init)
    | Key(w) ? Pop : (Init)
}
```

### Automate d’un zombie
```
zombie(Init) {
  *(Init) : 
    | Cell(F, A) ? Hit(F) : (Init)
    | Cell(B, A) ? Turn(B) : (Init)
    | Cell(R, A) ? Turn(R) : (Init)
    | Cell(L, A) ? Turn(L) : (Init)
    | Closest(A, N) ? Move(N) : (Init)
    | Closest(A, E) ? Move(E) : (Init)
    | Closest(A, S) ? Move(S) : (Init)
    | Closest(A, O) ? Move(O) : (Init)
    | Cell(N,V) & Cell(S,V) & Cell(E,V) & Cell(O,V) ? Move(N) / Move(S) / Move(O) / Move(E) / 10% Wait() : (Init)
    | Cell(N,O) & Cell(S,V) & Cell(E,V) & Cell(O,V) ? Move(S) / Move(E) / Move(O) / 10% Wait() : (Init)
    | Cell(N,V) & Cell(S,O) & Cell(E,V) & Cell(O,V) ? Move(N) / Move(E) / Move(O) / 10% Wait() : (Init)
    | Cell(N,V) & Cell(S,V) & Cell(E,O) & Cell(O,V) ? Move(N) / Move(S) / Move(O) / 10% Wait() : (Init)
    | Cell(N,V) & Cell(S,V) & Cell(E,V) & Cell(O,O) ? Move(N) / Move(S) / Move(E) / 10% Wait() : (Init)
    | Cell(N,O) & Cell(S,O) & Cell(E,V) & Cell(O,V) ? Move(E) /  Move(O) / 10% Wait()  : (Init)
    | Cell(N,V) & Cell(S,O) & Cell(E,O) & Cell(O,V) ? Move(N) /  Move(O) / 10% Wait()  : (Init)
    | Cell(N,V) & Cell(S,V) & Cell(E,O) & Cell(O,O) ? Move(N) /  Move(S) / 10% Wait()  : (Init)
    | Cell(N,O) & Cell(S,V) & Cell(E,V) & Cell(O,O) ? Move(S) /  Move(E) / 10% Wait()  : (Init)
    | Cell(N,V) & Cell(S,O) & Cell(E,O) & Cell(O,O) ? Move(N) / 10% Wait() : (Init)
    | Cell(N,O) & Cell(S,V) & Cell(E,O) & Cell(O,O) ? Move(S) / 10% Wait() : (Init)
    | Cell(N,O) & Cell(S,O) & Cell(E,V) & Cell(O,O) ? Move(E) / 10% Wait() : (Init)
    | Cell(N,O) & Cell(S,O) & Cell(E,O) & Cell(O,V) ? Move(O) / 10% Wait() : (Init)
}
```


### Automate d’un lapin (déplacement aléatoire)
```
Lapin(Init){
  * (Init): 
    | Cell(N,V) & Cell(S,V) & Cell(E,V) & Cell(O,V) ? Move(N) / Move(S) / Move(O) / Move(E) : (_)
    | Cell(N,O) & Cell(S,V) & Cell(E,V) & Cell(O,V) ? Move(S) / Move(E) / Move(O) : (_)
    | Cell(N,V) & Cell(S,O) & Cell(E,V) & Cell(O,V) ? Move(N) / Move(E) / Move(O) : (_)
    | Cell(N,V) & Cell(S,V) & Cell(E,O) & Cell(O,V) ? Move(N) / Move(S) / Move(O) : (_)
    | Cell(N,V) & Cell(S,V) & Cell(E,V) & Cell(O,O) ? Move(N) / Move(S) / Move(E) : (_)
    | Cell(N,O) & Cell(S,O) & Cell(E,V) & Cell(O,V) ? Move(E) /  Move(O)  : (_)
    | Cell(N,V) & Cell(S,O) & Cell(E,O) & Cell(O,V) ? Move(N) /  Move(O)  : (_)
    | Cell(N,V) & Cell(S,V) & Cell(E,O) & Cell(O,O) ? Move(N) /  Move(S)  : (_)
    | Cell(N,O) & Cell(S,V) & Cell(E,V) & Cell(O,O) ? Move(S) /  Move(E)  : (_)
    | Cell(N,V) & Cell(S,O) & Cell(E,O) & Cell(O,O) ? Move(N) : (_)
    | Cell(N,O) & Cell(S,V) & Cell(E,O) & Cell(O,O) ? Move(S) : (_)
    | Cell(N,O) & Cell(S,O) & Cell(E,V) & Cell(O,O) ? Move(E) : (_)
    | Cell(N,O) & Cell(S,O) & Cell(E,O) & Cell(O,V) ? Move(O) : (_)
  * (wait):
    | True ? : (_)
}
```

### Automate d’un piège
```
Piege(init){
  *(init):  
    | closest(A) ? pop() :(attendre)
  *(attendre)
    | wait() : (enclenché)
  * (enclenché):
    | wizz() : ()
}
```

### Automate d’un décor (buisson, arbre…)
```
Decor(Init){
  * (init): True ? : (init)
}
```

 
---
   AUTHORS: Florian Chappaz, Arthur Palma, Hugo Meignen, Chemsseddine Hadiby, Tom Kacha, Abdourahamane Mahaman Noury, Polytech'Grenoble, Univ. Grenoble Alpes
   DATE: juin 2021
