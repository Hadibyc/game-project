# Journal de bord, Equipe 1

## Lundi 14 Juin

### Répartition des tâches (matin)

- Florian + Abdourahamane : Automates et parcours des AST
- Chemsseddine : Prototypes de la Vue (Affichage des cases + Deux viewports), Conception de la fenêtre du menu
- Tom + Hugo + Arthur : Reconstrution du diagramme de classes, puis implémentation du modèle (Grille + classes des Entités + fonctions liées aux automates)

### Résumé de la matinée

#### Automates / AST

- Compréhension de la structure de l'AST et du fonctionnement du Visitor.
- Création de la classe AutBuilder (qui implémente IVisitor) et implémentation des fonctions visit pour Category, Direction et Key.
- Création des classes équivalentes pour l'automate (Aut_Category, Aut_Direction, Aut_Key).

#### Vue

- Mouvement du chasseur
- Deux viewports
- Création des cases

#### Modèle

- Diagramme de classes repensé
- Création d'une grille d'Entités
- Répartitions des tâches entre les trois contributeurs

### Résumé de l'après-midi

#### Automates / AST

- [Florian] Continue d'implémenter AutBuilder (environ la moitié des fonctions Visitor et des classes correspondantes faites)

#### Vue

- [Chemsseddine] Gestions des mouvements dans chaque viewports
- [Chemsseddine] A commencé à étudier la représentation du mouvement aléatoire d'un zombie

#### Controlleur

- [Abdourahamane] A commencé à associer les touches appuyés à l'entité concernée

#### Modèle

- [Arthur] Implémentation des classes relatives au décor : Decor , Bush, Tree, Rock, Lapin
- [Arthur] Implémentation des classes relatives aux superpouvoirs : SuperPower et Heal 
- [Hugo] Implémentation des classes relatives aux zombies : ZombieDay, ZombieNight 
- [Tom] Implémentation des classes relatives aux joueurs : Hunter, Hider, Prey, Werewolf 
- [Arthur + Hugo + Tom] Implémentation de la génération aléatoire du décor dans la grille du modèle (ModelGrid)


## Mardi 15 Juin

### Répartition des tâches

- Florian toujours sur la fabrication de AutBuilder et des classes associées : l'objectif est de finir aujourd'hui.
- Chemsseddine continuera de travailler sur la vue et commencera a échanger du code avec le modèle pour produire l'affichage correspondant.
- Tom, Arthur, Hugo et Abdourahamane continueront à se diviser et implémenter les nombreuses fonctions du modèle.

### Résumé de la matinée

#### Automates

- [Florian] Toutes les classes nécessaires ont été implémentées : il reste simplement à gérer les probabilités pour les actions et à implémenter AutBuilder (IVisitor).

#### Vue + Controlleur

- [Chemsseddine] Appui sur deux touches du clavier possibles et différenciées en fonction du joueur.
- [Chemsseddine] Création et affichage des bots
- [Chemsseddine] Début de l'implémentation du menu

#### Modèle

- [Abdourahamane] Fonction Wait des Entités.
- [Abdourahamane] Pop et Wizz des pièges.
- [Abdourahamane] Ajout de champ m_blocked dans Entity pour le piège.
- [Tom] Adaptation par rapport au changement des classes Aut_Category et Aut_Direction (par rapport à AutBuilder)
- [Hugo] Implémentation de Cell et Closest
- Ajout du champ m_avatar en accord avec la vue

### Résumé de l'après-midi

#### Automates

- [Florian] AutBuilder fonctionnelle : tests sur des fichiers .gal simple, notamment un automate de déplacement aléatoire modulable en probabilité.
- [Florian] Question posée à M. Périn : fonction enter() non implémentée, est-ce un problème ?
- [Florian] Il faudra aussi tester les opérateurs de conditions !
- [Florian] Prochaine étape : récupérer le code du Modèle pour des tests plus approfondis.
- [Abdourahamane] Correction et simplification des automates en .gal

#### Vue 

- [Chemsseddine] Intégration des sprites pour les actions

#### Modèle

- [Tom + Chemsseddine] Travail sur le regroupement du code de la partie modèle avec la partie vue.
- [Tom] Corrections diverses de warnings.
- [Abdourahamane] Correction des fonctions associées aux automates en fonction des modifications apportées aux .gal.
- [Hugo] Implémentation de la fonction Wizz du traquée qui permet de prendre/ quitter la possession d’un objet + modification des classes abstract de Pop, Hit et Power dans les classes Player et sa descendance.
- [Arthur] Implémentation des fonctions Hit et Hurt, permettant respectivement de frapper son adversaire, et de retirer des points de vie à celui qui encaisse une attaque. Légère correction de la fonction Closest.

## Mercredi 16 Juin

#### Automates

- [Florian] Dernières corrections, partie automate terminée, en attente de tests plus conséquents.

#### Automates + Modèle

- [Florian + Tom] Merge des parties automates et modèle.

#### Modèle

- [Tom + Hugo] Tests et correction de la fonction Move et des 3-4 fonctions qui en découlent. Grosse réflexion sur une méthode Closest marchant sur une map à effet sans bord.
- [Tom] Implémentation de la fonction closest et de ses tests et tests vérifiés après modification.
- [Hugo] Implémentation des tests de Wizz (quitter/prendre possession) du Hider et Prey et tests vérifiés après modification.

#### Vue

- [Chemsseddine] Réflexion sur la double caméra et de la gestion de l'effet carte qui se replie sur les bords.

## Jeudi 17 Juin

- Mise en commun Vue + Modèle + Automates + Controlleur

## Vendredi 18 Juin

- Mise en commun + correction de conflits diverses
- [Florian + Hugo] Réflexion + implémentation des timers
- [Chemsseddine + Tom] Avancée et correction de la partie Vue
- [Arthur + Abdourahamane] Implémentation pouvoir + pièges
