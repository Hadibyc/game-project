package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Direction;


public class Hit implements IAction {

	Aut_Direction m_dir;
	
	public Hit() {
		m_dir = new Aut_Direction("F");
	}
	
	public Hit(Aut_Direction dir) {
		m_dir = dir;
	}

	@Override
	public void apply(Entity e) {
		e.Hit(m_dir);
	}
	
}
