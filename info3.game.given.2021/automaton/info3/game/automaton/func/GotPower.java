package info3.game.automaton.func;

import info3.game.Entity;

public class GotPower implements ICondition {

	@Override
	public boolean eval(Entity e) {
		return e.GotPower();
	}

}
