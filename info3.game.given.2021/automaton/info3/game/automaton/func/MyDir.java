package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Direction;


public class MyDir implements ICondition {
	
	Aut_Direction m_dir;
	
	public MyDir(Aut_Direction dir) {
		m_dir = dir;
	}

	@Override
	public boolean eval(Entity e) {
		return e.MyDir(m_dir);
	}

}
