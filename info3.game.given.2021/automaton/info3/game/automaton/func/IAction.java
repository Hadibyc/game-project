package info3.game.automaton.func;

import info3.game.Entity;

public interface IAction {
	void apply(Entity e);
}
