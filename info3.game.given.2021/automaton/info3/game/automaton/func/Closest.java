package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;


public class Closest implements ICondition {
	Aut_Category m_cat;
	Aut_Direction m_dir;
	
	public Closest(Aut_Category cat, Aut_Direction dir) {
		m_cat = cat;
		m_dir = dir;
	}

	@Override
	public boolean eval(Entity e) {
		return e.Closest(m_cat, m_dir);
	}
	
}
