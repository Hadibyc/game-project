package info3.game.automaton.func;

import info3.game.Entity;

public class GotStuff implements ICondition {

	@Override
	public boolean eval(Entity e) {
		return e.GotStuff();
	}

}
