package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Direction;


public class Move implements IAction {
	Aut_Direction m_direction;
	
	public Move() {
		m_direction = new Aut_Direction("F");
	}
	
	public Move(Aut_Direction dir) {
		m_direction = dir;
	}

	@Override
	public void apply(Entity e) {
		e.Move(m_direction);
	}
	
	public String toString() {
		return "Move(" + ((m_direction != null) ? m_direction.toString() : "") + ")";
	}

}
