package info3.game.automaton.func;

import info3.game.Entity;

public interface ICondition {
	boolean eval(Entity e);
}
