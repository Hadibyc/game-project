package info3.game.automaton.func;

import info3.game.Entity;

public class True implements ICondition {

	@Override
	public boolean eval(Entity e) {
		return true;
	}
	
	public String toString() {
		return "True";
	}
}
