package info3.game.automaton.func;

import info3.game.Entity;

public class Wait implements IAction {

	@Override
	public void apply(Entity e) {
		e.Wait();
	}
	
}
