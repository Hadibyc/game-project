package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Direction;


public class Egg implements IAction {
	
	Aut_Direction m_dir;
	
	public Egg() {
		m_dir = new Aut_Direction("F");
	}
	
	public Egg(Aut_Direction dir) {
		m_dir = dir;
	}

	@Override
	public void apply(Entity e) {
		e.Egg(m_dir);
	}

}
