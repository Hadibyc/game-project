package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;


public class Cell implements ICondition {
	Aut_Direction m_direction;
	Aut_Category m_category;
	
	public Cell(Aut_Direction dir, Aut_Category cat) {
		m_direction = dir;
		m_category = cat;
	}

	@Override
	public boolean eval(Entity e) {
		return e.Cell(m_direction, m_category);
	}

	@Override
	public String toString() {
		return "Cell [m_direction=" + m_direction.toString() + ", m_category=" + m_category.toString() + "]";
	}
	
	
}
