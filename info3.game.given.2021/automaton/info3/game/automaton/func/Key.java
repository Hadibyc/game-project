package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Key;


public class Key implements ICondition {
	
	Aut_Key m_key;
	
	public Key(Aut_Key key) {
		m_key = key;
	}

	@Override
	public boolean eval(Entity e) {
		return e.Key(m_key);
	}

}
