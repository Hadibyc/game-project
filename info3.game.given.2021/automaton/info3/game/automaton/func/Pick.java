package info3.game.automaton.func;

import info3.game.Entity;
import info3.game.automaton.builder.Aut_Direction;


public class Pick implements IAction {

	Aut_Direction m_dir;
	
	public Pick() {
		m_dir = new Aut_Direction("F");
	}
	
	public Pick(Aut_Direction dir) {
		m_dir = dir;
	}

	@Override
	public void apply(Entity e) {
		e.Pick(m_dir);
	}
	
}
