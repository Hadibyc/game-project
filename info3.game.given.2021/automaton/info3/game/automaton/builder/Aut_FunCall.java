package info3.game.automaton.builder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import info3.game.Entity;
import info3.game.automaton.func.IAction;
import info3.game.automaton.func.ICondition;


public class Aut_FunCall extends Aut_Expression implements ICondition {
	String m_name;
	Aut_Direction m_direction;
	Aut_Category m_cat;
	Aut_Key m_key;
	List<Object> m_parameters;
	int m_percent;
	
	public static final int NO_PERCENT = -1;
	
	public Aut_FunCall(String name, List<Object> parameters, int percent) {
		m_name = "info3.game.automaton.func." + name;
		m_parameters = parameters;
		m_percent = percent;
	}
	
	public boolean eval(Entity e) {
		try {
			Class<?> funcType = Class.forName(m_name);
			Constructor<?> ctor ;
			Object function = null;
			if (m_parameters == null) {
				ctor = funcType.getConstructor();
				function = ctor.newInstance();
			}
			else {
				switch (m_parameters.size()) {
				case 0:
					ctor = funcType.getConstructor();
					function = ctor.newInstance();
					break;
				case 1:
					ctor = funcType.getConstructor(m_parameters.get(0).getClass());
					function = ctor.newInstance(m_parameters.get(0));
					break;
				case 2:
					ctor = funcType.getConstructor(m_parameters.get(0).getClass(), m_parameters.get(1).getClass());
					function = ctor.newInstance(m_parameters.get(0), m_parameters.get(1));
					break;
				default:
					throw new IllegalArgumentException("Illegal number of arguments for a FunCall");
				}
			}
			
			if (function instanceof IAction) {
				((IAction) function).apply(e);
			}
			else
				return ((ICondition) function).eval(e);
						
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		}
		
		return false;
	}
}
