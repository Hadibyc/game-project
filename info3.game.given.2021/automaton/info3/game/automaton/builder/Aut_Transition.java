package info3.game.automaton.builder;

import info3.game.Entity;

public class Aut_Transition {
	Aut_State m_targetState;
	Aut_Condition m_condition;
	Aut_Action m_action;
	
	public Aut_Transition(Aut_Condition condition, Aut_Action action, Aut_State target_state) {
		m_condition = condition;
		m_action = action;
		m_targetState = target_state;
	}
	
	public boolean evalCondition(Entity e) {
		return m_condition.eval(e);
	}
	
	public void doAction(Entity e) {
		m_action.apply(e);
	}
}
