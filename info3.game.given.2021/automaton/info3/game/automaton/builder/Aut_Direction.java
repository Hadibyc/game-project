package info3.game.automaton.builder;

public class Aut_Direction extends Aut_Terminal {

	public Aut_Direction(String content) {
		super(content);
	}

	@Override
	public boolean equals(Aut_Terminal t) {
		if (!(t instanceof Aut_Direction))
			return false;
		return m_content.equals(t.toString());
	}

}
