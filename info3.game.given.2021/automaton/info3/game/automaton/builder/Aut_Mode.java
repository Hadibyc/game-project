package info3.game.automaton.builder;

import info3.game.Entity;

public class Aut_Mode {
	Aut_State m_initialState;
	Aut_Behaviour m_behaviour;
	
	public Aut_Mode(Aut_State state, Aut_Behaviour behaviour) {
		m_initialState = state;
		m_behaviour = behaviour;
	}
	
	public void apply(Entity e) {
		if (e.getCurrentState().equals(m_initialState)) {
			m_behaviour.apply(e);
		}
	}
}
