package info3.game.automaton.builder;

public class Aut_Value {
	int m_value;

	public Aut_Value(int value) {
		m_value = value;
	}

	public String toString() {
		return Integer.toString(m_value);
	}
}
