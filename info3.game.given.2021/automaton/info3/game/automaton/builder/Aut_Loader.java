package info3.game.automaton.builder;

import java.util.List;

import info3.game.automata.ast.AST;
import info3.game.automata.parser.AutomataParser;

public class Aut_Loader {
	String m_filename;

	public Aut_Loader(String filename) {
		m_filename = filename;
	}

	public List<Aut_Automaton> loadAutomata() {
		try {
			AST ast = (AST) AutomataParser.from_file(m_filename);
			List<Aut_Automaton> automata = (List<Aut_Automaton>) ast.accept(new Aut_Builder());
			return automata;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
//	public Aut_Automaton loadAutomata() {
//		try {
//			AST ast = (AST) AutomataParser.from_file(m_filename);
//			Aut_Automaton automata = (Aut_Automaton) ast.accept(new Aut_Builder());
//			return automata;
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			return null;
//		}
//	}
	
}
