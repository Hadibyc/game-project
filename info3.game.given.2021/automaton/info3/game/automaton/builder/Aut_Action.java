package info3.game.automaton.builder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import info3.game.Entity;



public class Aut_Action {
	List<Aut_FunCall> m_funcalls;
	private TreeMap<Integer, Aut_FunCall> m_funIntervalls; // Ordered list of (n, f) where f is a funcall and n is a number representing a value in [0,100]
	
	public Aut_Action(List<Object> funcalls) {
		m_funcalls = new LinkedList<Aut_FunCall>();
		for (Object funcall : funcalls) {
			m_funcalls.add((Aut_FunCall) funcall);
		}
		m_funIntervalls = new TreeMap<Integer, Aut_FunCall>();
		
		int reservedPercent = 0;
		
		List<Aut_FunCall> noPercentFuncalls = new LinkedList<Aut_FunCall>();
		
		for (Aut_FunCall funcall : m_funcalls) {
			if (funcall.m_percent == Aut_FunCall.NO_PERCENT) {
				noPercentFuncalls.add(funcall);
			}
			else {
				reservedPercent += funcall.m_percent;
				m_funIntervalls.put(reservedPercent, funcall);
			}
		}
		
		if (noPercentFuncalls.size() != 0) {
			int availablePercent = 100 - reservedPercent;
			int commonPercent = availablePercent / noPercentFuncalls.size();
			int rest = availablePercent - commonPercent * noPercentFuncalls.size();
			
			if (commonPercent != 0) {
				for (Aut_FunCall funcall : noPercentFuncalls) {
					reservedPercent += commonPercent;
					if (rest != 0) {
						reservedPercent++;
						rest--;
					}
					m_funIntervalls.put(reservedPercent, funcall);
				}
			}
		}		
		
	}
	
	public void apply(Entity e) {
		Random rand = new Random();
		int randInt = rand.nextInt(100);
		
		for (Map.Entry<Integer, Aut_FunCall> entry : m_funIntervalls.entrySet()) {
			if (randInt + 1 <= (Integer) entry.getKey()) {
				((Aut_FunCall) entry.getValue()).eval(e) ;
				break;
			}
		}
	}
}
