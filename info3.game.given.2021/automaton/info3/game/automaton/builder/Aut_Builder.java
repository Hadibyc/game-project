package info3.game.automaton.builder;

import java.util.List;

import info3.game.automata.ast.AST;
import info3.game.automata.ast.Action;
import info3.game.automata.ast.Automaton;
import info3.game.automata.ast.Behaviour;
import info3.game.automata.ast.BinaryOp;
import info3.game.automata.ast.Category;
import info3.game.automata.ast.Condition;
import info3.game.automata.ast.Direction;
import info3.game.automata.ast.FunCall;
import info3.game.automata.ast.IVisitor;
import info3.game.automata.ast.Key;
import info3.game.automata.ast.Mode;
import info3.game.automata.ast.State;
import info3.game.automata.ast.Transition;
import info3.game.automata.ast.UnaryOp;
import info3.game.automata.ast.Underscore;
import info3.game.automata.ast.Value;

public class Aut_Builder implements IVisitor {
	
	@Override
	public Object visit(Category cat) {
		return new Aut_Category(cat.toString());
	}

	@Override
	public Object visit(Direction dir) {
		return new Aut_Direction(dir.toString());
	}

	@Override
	public Object visit(Key key) {
		return new Aut_Key(key.toString());
	}

	@Override
	public Object visit(Value v) {
		return new Aut_Value(v.value);
	}

	@Override
	public Object visit(Underscore u) {
		return new Aut_Underscore();
	}

	@Override
	public void enter(FunCall funcall) {
	}

	@Override
	public Object exit(FunCall funcall, List<Object> parameters) {
		return new Aut_FunCall(funcall.name, parameters, funcall.percent);
	}

	@Override
	public Object visit(BinaryOp operator, Object left, Object right) {
		return new Aut_BinaryOp(operator.operator, (Aut_Expression) left, (Aut_Expression) right);
	}

	@Override
	public Object visit(UnaryOp operator, Object expression) {
		return new Aut_UnaryOp(operator.operator, (Aut_Expression) expression);
	}

	@Override
	public Object visit(State state) {
		return new Aut_State(state.name);
	}

	@Override
	public void enter(Mode mode) {
	}

	@Override
	public Object exit(Mode mode, Object source_state, Object behaviour) {
		return new Aut_Mode((Aut_State) source_state, (Aut_Behaviour) behaviour);
	}

	@Override
	public Object visit(Behaviour behaviour, List<Object> transitions) {
		return new Aut_Behaviour(transitions);
	}

	@Override
	public void enter(Condition condition) {
	}

	@Override
	public Object exit(Condition condition, Object expression) {
		return new Aut_Condition((Aut_Expression) expression);
	}

	@Override
	public void enter(Action acton) {
	}

	@Override
	public Object exit(Action action, List<Object> funcalls) {
		return new Aut_Action(funcalls);
	}

	@Override
	public Object visit(Transition transition, Object condition, Object action, Object target_state) {
		return new Aut_Transition((Aut_Condition) condition, (Aut_Action) action, (Aut_State) target_state);
	}

	@Override
	public void enter(Automaton automaton) {
	}

	@Override
	public Object exit(Automaton automaton, Object initial_state, List<Object> modes) {
		return new Aut_Automaton((Aut_State) initial_state, modes);
	}

	@Override
	public Object visit(AST bot, List<Object> automata) {
		return automata;
	}

}
