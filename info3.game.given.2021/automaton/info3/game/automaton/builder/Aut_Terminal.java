package info3.game.automaton.builder;

public abstract class Aut_Terminal {
	String m_content;
	
	Aut_Terminal (String content) {
		m_content = content;
	}
	
	public abstract boolean equals(Aut_Terminal t);
	
	public String toString() {
		return m_content;
	}
}
