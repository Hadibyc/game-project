package info3.game.automaton.builder;

import java.util.LinkedList;
import java.util.List;

import info3.game.Entity;



public class Aut_Behaviour {
	List<Aut_Transition> m_transitions;
	
	public Aut_Behaviour(List<Object> transitions) {
		m_transitions = new LinkedList<Aut_Transition>();
		for (Object transition : transitions) {
			m_transitions.add((Aut_Transition) transition);
		}
	}
	
	public void apply(Entity e) {
		for (Aut_Transition transition : m_transitions) {
			if (transition.evalCondition(e)) {
				transition.doAction(e);
				break;
			}
		}
	}
}
