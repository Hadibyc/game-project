package info3.game.automaton.builder;

public class Aut_Key extends Aut_Terminal {

	public Aut_Key(String content) {
		super(content);
	}

	@Override
	public boolean equals(Aut_Terminal t) {
		if (!(t instanceof Aut_Key))
			return false;
		return t.toString().equals(m_content);
	}
	
}
