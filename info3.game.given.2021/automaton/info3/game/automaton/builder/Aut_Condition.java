package info3.game.automaton.builder;

import info3.game.Entity;
import info3.game.automaton.func.ICondition;


public class Aut_Condition implements ICondition {
	
	Aut_Expression m_expression;
	
	public Aut_Condition(Aut_Expression expression) {
		m_expression = expression;
	}

	@Override
	public boolean eval(Entity e) {
		return m_expression.eval(e);
	}

}
