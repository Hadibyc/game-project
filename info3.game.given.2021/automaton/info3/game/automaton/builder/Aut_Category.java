package info3.game.automaton.builder;

public class Aut_Category extends Aut_Terminal {
	
	public Aut_Category(String name) {
		super(name);
	}
	
	public boolean equals(Aut_Terminal t) {
		if (!(t instanceof Aut_Category))
			return false;
		if (m_content.equals(t.toString()))
			return true;
		return (m_content == "_" && t.toString() != "V") || (m_content != "V" && t.toString() == "_") || (m_content == "_" && t.toString() == "_");
	}
}
