package info3.game.automaton.builder;

import info3.game.Entity;
import info3.game.automaton.func.ICondition;


public class Aut_BinaryOp extends Aut_Expression implements ICondition {
	
	Aut_Expression m_left;
	Aut_Expression m_right;
	String m_op;
	
	public Aut_BinaryOp(String op, Aut_Expression left, Aut_Expression right) {
		m_left = left;
		m_right = right;
		m_op = op;
	}

	@Override
	public boolean eval(Entity e) {
		switch (m_op) {
		case "&":
			return m_left.eval(e) && m_right.eval(e);
		case "/":
			return m_left.eval(e) && !m_right.eval(e);
		default:
			throw new IllegalStateException("Illegal operator for Binary operation");
		}
	}

}
