package info3.game.automaton.builder;

public class Aut_State {
	String m_name;
	
	public Aut_State(String name) {
		m_name = name;
	}
	
	public boolean equals(Aut_State state) {
		return m_name.equals(state.m_name);
	}
}
