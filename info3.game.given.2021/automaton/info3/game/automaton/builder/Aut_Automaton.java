package info3.game.automaton.builder;

import java.util.LinkedList;
import java.util.List;

import info3.game.Entity;



public class Aut_Automaton {
	Aut_State m_initialState;
	List<Aut_Mode> m_modes;
	String m_name;
	
	public String toString() {
		return m_name;
	}

	public Aut_Automaton(Aut_State initialState, List<Object> modes) {
		m_initialState = initialState;
		m_modes = new LinkedList<Aut_Mode>();
		for (Object mode : modes) {
			m_modes.add((Aut_Mode) mode);
		}
	}
	
	public Aut_State getInitialState() {
		return m_initialState;
	}
	
	public void addMode(Aut_Mode mode) {
		m_modes.add(mode);
	}
	
	public void step(Entity e) {	
		for (Aut_Mode mode : m_modes) {
			if (mode.m_initialState.equals(m_initialState))
				mode.apply(e);
		}	
	}
}
