package info3.game.automaton.builder;

import info3.game.Entity;
import info3.game.automaton.func.ICondition;


public class Aut_UnaryOp extends Aut_Expression implements ICondition {
	String m_op;
	Aut_Expression m_cond;
	
	public Aut_UnaryOp(String op, Aut_Expression expression) {
		m_op = op;
		m_cond = expression;
	}

	@Override
	public boolean eval(Entity e) {
		if (m_op.equals("!"))
			return !m_cond.eval(e);
		else
			throw new IllegalStateException("Unary operation different of not");
	}
}
