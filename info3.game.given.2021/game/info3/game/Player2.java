package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;

public abstract class Player2 extends Player {

	Player2(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
	}
}