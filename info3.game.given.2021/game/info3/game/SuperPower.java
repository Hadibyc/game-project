package info3.game;

public abstract class SuperPower {
	protected static boolean available;

	public SuperPower() {
		available = false;
	}

	public static boolean getAvailable() {
		return available;
	}

	public static void setAvailable(boolean bool) {
		available = bool;
	}
	/*
	 * int identifier;
	 * 
	 * SuperPower(int identifier) { this.identifier = identifier; }
	 */
}
