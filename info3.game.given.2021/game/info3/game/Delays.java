package info3.game;

public class Delays {
	public static final int DECOR_DELAY = 100;
	public static final int HIDER_DELAY = 70;
	public static final int HUNTER_DELAY = 50;
	public static final int RABBIT_DELAY = 70;
	public static final int ZOMBIE_DELAY = 100;
}
