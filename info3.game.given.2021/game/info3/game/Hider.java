package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public class Hider extends Player2 {

	public Hider(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.HIDER_DELAY;// ajustable
		m_sprite = SpritesInstances.spHider;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
		m_possessionAnimation = new PossessionAnimation(m_modelGrid.m_game, 800, this);
		m_walkDirection = EAST;
		m_modelGrid.hiderx = x;
		m_modelGrid.hidery = y;
		old_x = x;
		old_y = y;
		Zombie.X_HIDER = x;
		Zombie.Y_HIDER = y;
		m_showen = m_modelGrid.m_game.m_isStarted;
	}

	@Override
	public void Wizz(Aut_Direction d) {
		if (!m_isMoving) {
			if (Cell(FRONT, V) && m_avatar == 0) {
				// cas ou il a l'apparance player et la case devant est vide => rien ne se
				// passse
				return;
			} else if (Cell(FRONT, T)) {
				// cas ou un zombie ou le chasseur (category T) est devant peut importe son
				// apparance => rien ne se passse
				return;
			}

			else if (Cell(FRONT, V) && m_avatar != 0) {
				// cas ou il est possede qq chose et en sort en laissant derriere lui l'element
				// qu'il possedait
				old_x = m_x;
				old_y = m_y;
				setQuitObstacle(m_avatar, old_x, old_y, null);
				m_sprite = SpritesInstances.spHider;
				m_speed = Delays.HIDER_DELAY;
				m_movement.updateSprite(this);
				m_possessionAnimation.updateSprite(this);
				m_avatar = 0;
			} else if (Cell(FRONT, O) && m_avatar == 0) {
				// cas ou il a l'apparance player et prend possesion de l'objet devant
				Entity E = getEntity(FRONT);
				m_movement.start();
				m_speed = Delays.DECOR_DELAY;
				if (E instanceof Rabbit && E.m_isWaiting) {
					this.setCoordFront(E);
					m_speed = Delays.RABBIT_DELAY;
					m_sprite = SpritesInstances.spRabbit;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 1;
				} else if (E instanceof Bush) {
					this.setCoordFront(E); // 1er freepos
					m_sprite = SpritesInstances.spBush;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 2;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else if (E instanceof Tree) {
					this.setCoordFront(E);
					m_sprite = SpritesInstances.spTree;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 3;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else if (E instanceof Rock) {
					this.setCoordFront(E);
					m_sprite = SpritesInstances.spRock;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 4;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else {
				}
			} else if (Cell(FRONT, O) && m_avatar != 0) {
				// cas ou il possede qq chose, quitte son objet actuel et possesion de l'objet
				// devant
				old_x = m_x;
				old_y = m_y;
				Entity E = getEntity(FRONT);
				m_movement.start();
				m_speed = Delays.DECOR_DELAY;
				if (E instanceof Rabbit && E.m_isWaiting) {
					setQuitObstacle(m_avatar, old_x, old_y, E);
					m_speed = Delays.RABBIT_DELAY;
					m_sprite = SpritesInstances.spRabbit;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 1;
				} else if (E instanceof Bush) {
					setQuitObstacle(m_avatar, old_x, old_y, E);
					m_sprite = SpritesInstances.spBush;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 2;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else if (E instanceof Tree) {
					setQuitObstacle(m_avatar, old_x, old_y, E);
					m_sprite = SpritesInstances.spTree;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 3;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else if (E instanceof Rock) {
					setQuitObstacle(m_avatar, old_x, old_y, E);
					m_sprite = SpritesInstances.spRock;
					m_movement.updateSprite(this);
					m_possessionAnimation.updateSprite(this);
					m_avatar = 4;
					m_possessionAnimation.start();
					m_isPossessing = true;
				} else {
				}
			}
		}
	}

	@Override
	public void invertRole() {
		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
		if (m_isMoving) {
			m_modelGrid.freePos(old_x, old_y);
			m_modelGrid.freePos(m_x, m_y);
		}
		this.m_blocked = true;
		WereWolf nightForm = new WereWolf(m_modelGrid, m_automaton, T, m_x, m_y);
		nightForm.m_game = m_modelGrid.m_game;
		nightForm.setHp(m_hp);
		nightForm.m_direction = m_direction;
		nightForm.m_movement.changeDirection();
		nightForm.m_keyWalk = this.m_keyWalk;
		nightForm.m_keyAction = this.m_keyAction;
		m_modelGrid.m_hider = nightForm;
		m_modelGrid.m_p2 = nightForm;
		if (m_modelGrid.automatedSettings[2].equals("J1")) {
			m_modelGrid.automatPlayer1.remove(this);
			m_modelGrid.automatPlayer1.add(nightForm);
		} else if (m_modelGrid.automatedSettings[2].equals("J2")) {
			m_modelGrid.automatPlayer2.remove(this);
			m_modelGrid.automatPlayer2.add(nightForm);
		}
	}

}
