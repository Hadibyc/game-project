package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;

public class ZombieDay extends Zombie {
	int m_choiceHandler = 500 + (int) (Math.random() * ((1000 - 500) + 1));
	int m_choiceAleatoire = 0;

	public ZombieDay(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.ZOMBIE_DELAY; //
		m_detection = 2; //
		m_sprite = SpritesInstances.spZombieDay;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
		m_hitAnimation = new HitAnimation(m_modelGrid.m_game, m_speed, this);
	}

	public void tick(long Elapsed) {
	}

	@Override
	public void setM_detection() {
		if (m_angry) {
			m_detection = 6;
			m_sprite = SpritesInstances.spZombieDayAngry;
			m_movement.updateSprite(this);
		} else {
			m_detection = 4;
			m_sprite = SpritesInstances.spZombieDay;
			m_movement.updateSprite(this);
		}
	}

	@Override
	protected void tick(long elapsed, int codePlayer1) {
	}

	@Override
	public void invertRole() {
		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
		if (m_isMoving)
			m_modelGrid.freePos(old_x, old_y);
		ZombieNight nightForm = new ZombieNight(m_modelGrid, m_automaton, m_category, m_x, m_y);
		nightForm.m_game = m_modelGrid.m_game;
		nightForm.setHp(m_hp);
		nightForm.m_direction = m_direction;
		nightForm.m_movement.changeDirection();
	}

}
