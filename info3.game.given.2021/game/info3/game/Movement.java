package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Movement extends Animation {

	int m_offsetx, m_offsety;

	private int m_maxIdx;
	private int m_spriteLine;
	private int m_spriteWidth;
	private int m_nbImg;

	public class MovementListener implements AnimationListener {
		@Override
		public void done(Animation a) {
			reinitAnimation();
		}
	}

	public Movement(Game g, int delay, Entity e, Sprite sprite) {
		super(g, delay, null);
		m_listener = new MovementListener();
		m_entity = e;
		m_sprite = sprite;
		m_spriteLine = 8;
		m_spriteWidth = 13;
		m_nbImg = 9;
		changeDirection();
		setPosition(m_entity.m_x, m_entity.m_y, 2);
	}

	@Override
	public void start() {
		m_entity.m_isMoving = true;
		super.start();
	}

	@Override
	public void setPosition(int x, int y, int scale) {
		m_x = x;
		m_y = y;
		m_offsetx = 0;
		m_offsety = 0;
		m_scale = scale;
	}

	@Override
	public boolean done() {
		return m_done;
	}

	@Override
	protected boolean nextImage() {
		m_idx++;
		if (m_idx < m_maxIdx) {
			if (m_idx == m_maxIdx - m_nbImg / 2) {
				if (!(m_entity.m_modelGrid.m_grid[m_entity.old_y][m_entity.old_x] instanceof Decor
						&& m_game.m_model.automatedSettings[3].equals("Decor")))
					m_entity.m_modelGrid.freePos(m_entity.old_x, m_entity.old_y);
				m_entity.m_modelGrid.setEntity(m_entity);
				m_offsetx = -m_offsetx;
				m_offsety = -m_offsety;
			}
			updateCoordinates();
			m_img = (BufferedImage) m_sprite.m_images[m_idx];
			return true;
		} else {
			m_done = true;
			m_entity.m_isMoving = false;
			return false;
		}
	}

	@Override
	protected void paint(Graphics g, int width, int height, int i, int j) {
		g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * i) + m_offsetx,
				((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * j) + m_offsety, m_scale * m_img.getWidth(),
				m_scale * m_img.getHeight(), null);
	}

	public void changeDirection() {
		switch (m_entity.m_direction.toString()) {
		case "N":
			m_idx = m_spriteWidth * m_spriteLine;
			m_maxIdx = m_spriteWidth * m_spriteLine + m_nbImg;
			break;
		case "W":
			m_idx = m_spriteWidth * (m_spriteLine + 1);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 1) + m_nbImg;
			break;
		case "S":
			m_idx = m_spriteWidth * (m_spriteLine + 2);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 2) + m_nbImg;
			break;
		case "E":
			m_idx = m_spriteWidth * (m_spriteLine + 3);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 3) + m_nbImg;
			break;
		}
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
	}

	private void updateCoordinates() {
		switch (m_entity.m_direction.toString()) {
		case "N":
			m_offsety = m_offsety - ((m_game.m_canvas.getHeight() / (2 * m_entity.m_modelGrid.m_zone + 1))) / m_nbImg;
			break;
		case "W":
			m_offsetx = m_offsetx
					- ((m_game.m_canvas.getWidth() / (2 * (2 * m_entity.m_modelGrid.m_zone + 1)))) / m_nbImg;
			break;
		case "S":
			m_offsety = m_offsety + ((m_game.m_canvas.getHeight() / (2 * m_entity.m_modelGrid.m_zone + 1))) / m_nbImg;
			break;
		case "E":
			m_offsetx = m_offsetx
					+ ((m_game.m_canvas.getWidth() / (2 * (2 * m_entity.m_modelGrid.m_zone + 1)))) / m_nbImg;
			break;
		}
	}

	private void reinitAnimation() {
		m_done = false;
		m_entity.m_modelGrid.setEntity(m_entity);
		setPosition(m_entity.m_x, m_entity.m_y, 2);
		changeDirection();
	}

}
