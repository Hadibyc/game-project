/**
 * 
 */
package info3.game;

/**
 * @author chemsou
 *
 */
public class Camera {

	private Game game;
	private float xOffset, yOffset;
	int EntityType;

	public Camera(Game game, float xOffset, float yOffset) {
		this.game = game;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	public void centerOnEntity(Entity e) {
		xOffset = e.getX() - game.m_canvas.getWidth() / 4 + e.m_spritew / 4;
		yOffset = e.getY() - game.m_canvas.getHeight() / 4 + e.m_spriteh / 4;
		if (e instanceof Hunter) {
			EntityType = 0;
		} else {
			EntityType = 1;
		}

	}

	public void move(float xAmt, float yAmt) {
		xOffset += xAmt;
		yOffset += yAmt;
	}

	public float getxOffset() {
		return xOffset;
	}

	public void setxOffset(float xOffset) {
		this.xOffset = xOffset;
	}

	public float getyOffset() {
		return yOffset;
	}

	public void setyOffset(float yOffset) {
		this.yOffset = yOffset;
	}
}
