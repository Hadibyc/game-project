package info3.game;

import java.awt.*;
import java.awt.image.BufferedImage;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Loader;

import java.lang.Math;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

public class ModelGrid extends JFrame {

	private static final long serialVersionUID = 1L;

	static int WIDTH = 40;
	static int HEIGHT = 40;

	int NB_BUSH;
	int NB_ROCK;
	int NB_TREE;
	int NB_ZOMBIE;
	int NB_RABBIT;
	int width, height, rows, columns;

	static final Aut_Category V = new Aut_Category("V");
	static final Aut_Category O = new Aut_Category("O");
	static final Aut_Category A = new Aut_Category("A");
	static final Aut_Category T = new Aut_Category("T");
	public static final Aut_Category G = new Aut_Category("G"); // case réservée
	public static final Aut_Category D = new Aut_Category("D"); // trap

	public Entity[][] m_grid;
	public Entity[][] m_gridDisplayLeft;
	public Entity[][] m_gridDisplayRight;
	public int m_zone = 6;
	Entity m_hider;
	Entity m_hunter;
	int hiderx;
	int hidery;
	int hunterx;
	int huntery;

	public SuperPower m_gamePower;
	public Player1 m_p1;
	public Player2 m_p2;
	public Game m_game;
	public String[] automatedSettings;

	public LinkedList<Entity> automatedEntity = new LinkedList<Entity>();
	public LinkedList<Entity> deadEntity = new LinkedList<Entity>();
	public LinkedList<Entity> addEntity = new LinkedList<Entity>();

	public Aut_Automaton m_autLapin;
	public Aut_Automaton m_autZombie;
	public Aut_Automaton m_autDecor;
	public Aut_Automaton m_autJ1;
	public Aut_Automaton m_autJ2;
	public Aut_Automaton m_autTrap;

	public LinkedList<Entity> automatPlayer1 = new LinkedList<Entity>();
	public LinkedList<Entity> automatPlayer2 = new LinkedList<Entity>();

	public ModelGrid(int w, int h, int[] settings, String[] automatSettings, Game game) {
		m_game = game;
		automatedSettings = automatSettings;
		m_autZombie = (Aut_Automaton) (new Aut_Loader("gal_examples/" + automatSettings[0] + ".gal")).loadAutomata()
				.get(0);
		m_autJ1 = (Aut_Automaton) (new Aut_Loader("gal_examples/" + automatSettings[1] + ".gal")).loadAutomata().get(0);
		m_autJ2 = (Aut_Automaton) (new Aut_Loader("gal_examples/" + automatSettings[2] + ".gal")).loadAutomata().get(0);
		m_autDecor = (Aut_Automaton) (new Aut_Loader("gal_examples/" + automatSettings[3] + ".gal")).loadAutomata()
				.get(0);
		m_autLapin = (Aut_Automaton) (new Aut_Loader("gal_examples/" + automatSettings[4] + ".gal")).loadAutomata()
				.get(0);
		m_autTrap = (Aut_Automaton) (new Aut_Loader("gal_examples/Trap.gal")).loadAutomata().get(0);
		setSize(width = w, height = h);
		WIDTH = settings[0];
		HEIGHT = settings[0];
		NB_ZOMBIE = settings[1];
		NB_BUSH = settings[2];
		NB_ROCK = settings[2];
		NB_TREE = settings[2];
		NB_RABBIT = settings[2];
		rows = WIDTH;
		columns = HEIGHT;

		m_grid = new Entity[HEIGHT][WIDTH];
		m_gridDisplayRight = new Entity[2 * m_zone + 1][2 * m_zone + 1];
		m_gridDisplayLeft = new Entity[2 * m_zone + 1][2 * m_zone + 1];
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				m_grid[j][i] = new Bush(this, m_autDecor, V, i, j);
			}
		}
		int x = (int) (WIDTH / 2);
		int y = (int) (HEIGHT / 2);
		m_gamePower = new Heal();
		Entity hunter = new Hunter(this, m_autJ1, T, x - 1, y);
		m_hunter = hunter;
		m_grid[y][x - 1] = hunter;
		((Player1) hunter).m_superPower = m_gamePower;
		hunter.m_game = m_game;
		if (automatedSettings[1].equals("J1"))
			automatPlayer1.add(hunter);
		else if (automatedSettings[1].equals("J2"))
			automatPlayer2.add(hunter);

		Entity hider = new Hider(this, m_autJ2, A, x, y);
		m_hider = hider;
		m_grid[y][x] = hider;
		hider.m_game = m_game;
		((Player2) hider).m_superPower = m_gamePower;
		if (automatedSettings[2].equals("J1"))
			automatPlayer1.add(hider);
		else if (automatedSettings[2].equals("J2"))
			automatPlayer2.add(hider);

		m_grid = ModelGridGenerator();
		this.preDisplaying(x - 1, y, 1);
		this.preDisplaying(x, y, 0);
	}

	public Entity[][] ModelGridGenerator() {
		Generator(NB_BUSH, "BUSH");
		Generator(NB_ROCK, "ROCK");
		Generator(NB_TREE, "TREE");
		Generator(NB_ZOMBIE, "ZOMBIE");
		Generator(NB_RABBIT, "RABBIT");
		return m_grid;
	}

	public void Generator(int n, String s) {
		for (int i = 0; i < n; i++) {
			int x = (int) (Math.random() * WIDTH);
			int y = (int) (Math.random() * HEIGHT);
			if (m_grid[y][x].m_category.equals(V)) {
				switch (s) {
				case "BUSH":
					Bush bush = new Bush(this, m_autDecor, O, x, y);
					if (automatedSettings[3].equals("J1"))
						automatPlayer1.add(bush);
					else if (automatedSettings[3].equals("J2"))
						automatPlayer2.add(bush);
					break;
				case "ROCK":
					Rock rock = new Rock(this, m_autDecor, O, x, y);
					if (automatedSettings[3].equals("J1"))
						automatPlayer1.add(rock);
					else if (automatedSettings[3].equals("J2"))
						automatPlayer2.add(rock);
					break;
				case "TREE":
					Tree tree = new Tree(this, m_autDecor, O, x, y);
					if (automatedSettings[3].equals("J1"))
						automatPlayer1.add(tree);
					else if (automatedSettings[3].equals("J2"))
						automatPlayer2.add(tree);
					break;
				case "ZOMBIE":
					ZombieDay zombie = new ZombieDay(this, m_autZombie, T, x, y);
					if (automatedSettings[1].equals("J1"))
						automatPlayer1.add(zombie);
					else if (automatedSettings[1].equals("J2"))
						automatPlayer2.add(zombie);
					break;
				case "RABBIT":
					Rabbit rabbit = new Rabbit(this, m_autLapin, O, x, y);
					if (automatedSettings[4].equals("J1"))
						automatPlayer1.add(rabbit);
					else if (automatedSettings[4].equals("J2"))
						automatPlayer2.add(rabbit);
					break;
				}
			} else
				i--;
		}
	}

	void freePos(int x, int y) {
		m_grid[y][x] = new Bush(this, m_autDecor, V, x, y);
	}

	void setEntity(Entity e) {
		m_grid[e.m_y][e.m_x] = e;
	}

	public String toString() {
		String gridStr = new String();
		for (int i = 0; i < HEIGHT; i++) {
			Entity[] row = m_grid[i];
			for (int j = 0; j < WIDTH; j++) {
				Entity point = row[j];
				switch (point.m_category.toString()) {
				case "V":
					gridStr += "-";
					break;
				case "O":
					gridStr += "O";
					break;
				case "T":
					gridStr += "T";
					break;
				case "A":
					gridStr += "A";
					break;
				case "G":
					gridStr += "G";
					break;
				case "D":
					gridStr += "D";
					break;
				default:
					throw new IllegalStateException();
				}
			}
			gridStr += "\n";
		}

		return gridStr;
	}

	public void preDisplaying(int x, int y, int a) {
		if (a == 0) {
			int refXLeft = Math.floorMod(x - m_zone, WIDTH);
			int refYLeft = Math.floorMod(y - m_zone, HEIGHT);
			for (int i = 0; i <= 2 * m_zone; i++) {
				for (int j = 0; j <= 2 * m_zone; j++) {
					m_gridDisplayLeft[j][i] = m_grid[Math.floorMod((j + refYLeft), HEIGHT)][Math
							.floorMod((i + refXLeft), WIDTH)];

				}
			}
		} else {
			int refXRight = Math.floorMod(x - m_zone, WIDTH);
			int refYRight = Math.floorMod(y - m_zone, HEIGHT);
			for (int i = 0; i <= 2 * m_zone; i++) {
				for (int j = 0; j <= 2 * m_zone; j++) {
					m_gridDisplayRight[j][i] = m_grid[Math.floorMod((j + refYRight), HEIGHT)][Math
							.floorMod((i + refXRight), WIDTH)];
				}
			}
		}

	}

	public void paint(Graphics g, int w, int h) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(2));
		g2.setColor(Color.RED.darker());
		width = w;
		height = h;
		BufferedImage grassImg;
		switch (m_game.remainingTime) {
		case 0:
			grassImg = m_game.m_isDay ? Images.grass10 : Images.grass1;
			break;
		case 1:
			grassImg = m_game.m_isDay ? Images.grass9 : Images.grass2;
			break;
		case 2:
			grassImg = m_game.m_isDay ? Images.grass8 : Images.grass3;
			break;
		case 3:
			grassImg = m_game.m_isDay ? Images.grass7 : Images.grass4;
			break;
		case 4:
			grassImg = m_game.m_isDay ? Images.grass6 : Images.grass5;
			break;
		case 5:
			grassImg = m_game.m_isDay ? Images.grass5 : Images.grass6;
			break;
		case 6:
			grassImg = m_game.m_isDay ? Images.grass4 : Images.grass7;
			break;
		case 7:
			grassImg = m_game.m_isDay ? Images.grass3 : Images.grass8;
			break;
		case 8:
			grassImg = m_game.m_isDay ? Images.grass2 : Images.grass9;
			break;
		case 9:
			grassImg = m_game.m_isDay ? Images.grass1 : Images.grass10;
			break;
		default:
			grassImg = m_game.m_isDay ? Images.grass1 : Images.grass10;
			break;
		}
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				g2.drawImage(grassImg, (int) (width / (2 * m_zone + 1) * i), (int) (height / (2 * m_zone + 1) * j),
						(int) (width / (2 * m_zone + 1)), (int) (height / (2 * m_zone + 1)), null);

			}
		}
		for (int i = 0; i < 2 * m_zone; i++) {
			for (int j = 0; j < 2 * m_zone; j++) {
				if (!this.m_gridDisplayLeft[j][i].m_category.equals(V)
						&& !this.m_gridDisplayLeft[j][i].m_category.equals(G)) {
					if (this.m_gridDisplayLeft[j][i] instanceof Hider || this.m_gridDisplayLeft[j][i] instanceof Prey) {
						if ((this.m_hider.m_isOnTrap == false))
							this.m_gridDisplayLeft[j][i].paint(g, width, height, i, j);
					} else {
						this.m_gridDisplayLeft[j][i].paint(g, width, height, i, j);
					}

				}
				if (!this.m_gridDisplayRight[j][i].m_category.equals(V)
						&& !this.m_gridDisplayRight[j][i].m_category.equals(G)) {
					if (this.m_hider.m_showen == true) {
						if (this.m_gridDisplayRight[j][i] instanceof Hider
								|| this.m_gridDisplayRight[j][i] instanceof Prey) {
							if ((this.m_hider.m_isOnTrap == false))
								this.m_gridDisplayRight[j][i].paint(g, width, height, i + (2 * m_zone + 1), j);
						} else {
							this.m_gridDisplayRight[j][i].paint(g, width, height, i + (2 * m_zone + 1), j);
						}

					} else {
						if (!(this.m_gridDisplayRight[j][i] instanceof Hider)) {
							this.m_gridDisplayRight[j][i].paint(g, width, height, i + (2 * m_zone + 1), j);
						}
					}

				}
			}
		}

	}

}
