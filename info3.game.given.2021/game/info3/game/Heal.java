package info3.game;

public class Heal extends SuperPower {
	
	public Heal() {
		super();
	}
	 
	public static void effect(Player player) {
		if (player.gotPower()) {
			player.m_hp += 30;
			player.setGotPower(false);
		}
		
	}

}
