/**
 * 
 */
package info3.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author chemsou
 *
 */
public class GameOver extends JFrame implements ActionListener {
	int m_winner;
	Game m_game;
	JFrame m_frame;
	JButton bouton1;
	JButton bouton2;
	JLabel text;

	GameOver(int Winner, Game g) {
		m_winner = Winner;
		m_game = g;
		m_game.stop();
	}

	public void reply() {
		m_frame = new JFrame();
		m_frame.setTitle("Game finished");
		m_frame.setLayout(new BorderLayout());
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("resources/go.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		text = new JLabel(new ImageIcon(img));
		text.setHorizontalTextPosition(JLabel.CENTER);
		if (m_winner == 1)
			text.setText("        Player one won , do you wanna play again ?  ");

		if (m_winner == 2)
			text.setText("        Player two won , do you wanna play again ?  ");
		if (m_winner == 0)
			text.setText("        draw!!, do you wanna play again ?  ");
		text.setFont(new Font(Font.SERIF, Font.BOLD, 30));
		text.setForeground(Color.WHITE);
		text.setBounds(50, 100, 100, 100);
		m_frame.getContentPane().add(text);
		m_frame.setContentPane(text);
		bouton1 = new JButton("REPLAY");
		bouton2 = new JButton("EXIT");

		buttcreator(bouton1);
		bouton1.setBounds(50, 450, 100, 100);
		buttcreator(bouton2);
		bouton2.setBounds(850, 450, 100, 100);
		text.add(bouton1);
		text.add(bouton2);

		bouton1.addActionListener(this);
		bouton2.addActionListener(this);
		m_frame.setResizable(false);
		m_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_frame.setSize(1000, 600);
		m_frame.setLocationRelativeTo(null);
		m_frame.setVisible(true);

	}

	public static void buttcreator(JButton but) {
		but.setFont(new Font("Gill Sans MT", Font.BOLD, 14));
		but.setBackground(new Color(0x2dce98));
		but.setForeground(Color.white);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if (source.equals(bouton2)) {
			System.exit(0);
		}
		if (source.equals(bouton1)) {
			m_frame.setVisible(false);
			GameMenu m_reply = new GameMenu();
			m_reply.build();
		}

	}

}
