/**
 * 
 */
package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import info3.game.Animation.AnimationListener;
import info3.game.DieAnimation.DieListener;

/**
 * @author chemsou
 *
 */
public class TrapAnimation extends Animation {
	private int m_maxIdx;
	private int m_spriteLine = 6;
	private int m_spriteWidth = 5;
	private int m_nbImg = 0;
	Game m_game;

	TrapAnimation(Game g, int delay, Entity e) {
		super(g, delay, null);
		m_game = g;
		m_listener = new TrapListener();
		m_entity = e;
		m_sprite = e.m_sprite;
		m_idx = 0;
		m_maxIdx = m_spriteWidth * m_spriteLine + m_nbImg;
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
		setPosition(m_entity.m_x, m_entity.m_y, 1);
		// TODO Auto-generated constructor stub
		super.start();
	}

	public class TrapListener implements AnimationListener {
		@Override
		public void done(Animation a) {
			((Trap) m_entity).end();
		}
	}

	@Override
	public void setPosition(int x, int y, int scale) {
		// TODO Auto-generated method stub
		m_x = x;
		m_y = y;
		m_scale = scale;

	}

	@Override
	public boolean done() {
		return m_done;
	}

	@Override
	protected boolean nextImage() {
		m_idx++;
		if (m_idx < m_maxIdx) {
			m_img = (BufferedImage) m_sprite.m_images[m_idx];
			return true;
		} else {
			m_done = true;
			m_listener.done(this);
			return false;
		}
	}

	@Override
	protected void paint(Graphics g, int width, int height, int i, int j) {
		g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * (i + 1)) - (m_img.getWidth() / 2),
				((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * (j)) - (m_img.getHeight() / 2),
				m_scale * m_img.getWidth(), m_scale * m_img.getHeight(), null);

	}

}
