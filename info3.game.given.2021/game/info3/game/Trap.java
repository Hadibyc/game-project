package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public class Trap extends Entity {
	public boolean m_used = false;
	TrapAnimation m_trapAnimation;
	boolean created = true;

	Trap(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_game = m_modelGrid.m_game;
		m_used = false;
		m_sprite = SpritesInstances.spTrap;
	}

	// il n'y a pas besoin de direction car le piege n'est que sur
	// une seule case en m_x et m_y
	@Override
	public boolean Closest(Aut_Category m_cat, Aut_Direction m_dir) {
		if (m_game.m_nbManchesCourantes <= 2) {
			int refX = Math.floorMod(getX() - 1, ModelGrid.WIDTH);
			int refY = Math.floorMod(getY() - 1, ModelGrid.HEIGHT);
			for (int i = 0; i <= 2; i++) {
				for (int j = 0; j <= 2; j++) {
					if (m_modelGrid.m_grid[Math.floorMod((j + refY), ModelGrid.HEIGHT)][Math.floorMod((i + refX),
							ModelGrid.WIDTH)].m_category.equals(A)) {
						return true;
					}
				}
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	protected void tick(long elapsed, int codePlayer1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void Pop(Aut_Direction d) {
		if (created) {
			created = false;
			m_modelGrid.m_hider.m_isOnTrap = true;
			m_trapAnimation = new TrapAnimation(m_modelGrid.m_game, 300, this);
		}

	}

	public void end() {
		if (m_game.m_isDay) {
			m_modelGrid.m_hider.m_blocked = false;
			m_modelGrid.m_hider.m_isOnTrap = false;
		} else {
			m_modelGrid.m_p1.m_showen = true;
			m_modelGrid.m_p1.m_blocked = false;
			m_modelGrid.m_p1.m_isOnTrap = false;
			m_modelGrid.m_hider.m_blocked = false;
			m_modelGrid.m_hider.m_isOnTrap = false;
		}

		this.m_used = true;
		m_modelGrid.freePos(m_x, m_y);
	}

	@Override
	public void Wizz(Aut_Direction d) {
		Turn(d);
	}

	public void remove() {
//		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
	}

	@Override
	public void paint(Graphics g, int width, int height, int i, int j) {
		if (m_game.m_nbManchesCourantes <= 2) {

			if (this.m_used == false) {
				if (m_modelGrid.m_hider.m_isOnTrap) {
					if (m_game.m_isDay) {
						m_modelGrid.m_hider.m_blocked = true;
					} else {
						m_modelGrid.m_p1.m_blocked = true;
					}

					for (int k = 1; k < 2 * m_modelGrid.m_zone; k++) {
						for (int d = 1; d < 2 * m_modelGrid.m_zone; d++) {
							if (m_modelGrid.m_gridDisplayLeft[d][k] instanceof Hider
									|| m_modelGrid.m_gridDisplayLeft[d][k] instanceof Prey) {
								if (m_trapAnimation != null)
									m_trapAnimation.paint(g, width, height, k, d);
							}
							if (m_modelGrid.m_gridDisplayRight[d][k] instanceof Hider
									|| m_modelGrid.m_gridDisplayRight[d][k] instanceof Prey) {
								if (m_trapAnimation != null)
									m_trapAnimation.paint(g, width, height, k + (2 * m_modelGrid.m_zone + 1), d);
							}
						}
					}
				} else {
					for (int k = 1; k < 2 * m_modelGrid.m_zone; k++) {
						for (int d = 1; d < 2 * m_modelGrid.m_zone; d++) {
							if (m_game.m_isDay) {
								if (m_modelGrid.m_gridDisplayRight[d][k].m_category.equals(D)) {
									m_sprite = SpritesInstances.spStar;
									BufferedImage m_img = (BufferedImage) m_sprite.m_images[0];
									g.drawImage(m_img,
											((width / (2 * (2 * m_modelGrid.m_zone + 1)))
													* (k + (2 * m_modelGrid.m_zone + 1))) + 50,
											((height / (2 * m_modelGrid.m_zone + 1)) * d) + 50, 50, 50, null);
									m_sprite = SpritesInstances.spTrap;
								}
							} else {
								if (m_modelGrid.m_gridDisplayLeft[d][k].m_category.equals(D)) {
									m_sprite = SpritesInstances.spStar;
									BufferedImage m_img = (BufferedImage) m_sprite.m_images[0];
									g.drawImage(m_img, (width / (2 * (2 * m_modelGrid.m_zone + 1))) * k + 50,
											(height / (2 * m_modelGrid.m_zone + 1)) * d + 50, 50, 50, null);
									m_sprite = SpritesInstances.spTrap;
								}
							}
						}
					}
				}
			}

		}
	}
}
