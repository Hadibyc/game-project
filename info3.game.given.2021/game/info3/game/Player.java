package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public abstract class Player extends Entity {

	int m_damage;
	public int m_avatar; // 0 -> normal / 1 -> lapin / 2 -> buisson / 3 -> arbre / 4 -> caillou
	public boolean m_gotPower;
	public SuperPower m_superPower;

	public boolean gotPower() {
		return m_gotPower;
	}

	public void setGotPower(boolean m_hasPower) {
		this.m_gotPower = m_hasPower;
	}

	public boolean signalToGetPower() {
		return SuperPower.getAvailable();
	}

	@Override
	public void Power() {
		if (gotPower()) {
			Heal.effect(this);
			if (this instanceof Player1) {
				m_game.proj1.setValue(this.m_hp);
			}
			if (this instanceof Player2) {
				m_game.proj2.setValue(this.m_hp);
			}
		}
	}

	public void recupPower() {
		if (signalToGetPower()) {
			setGotPower(true);
			m_superPower.setAvailable(false);
		}
	}

	Player(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_hp = 100; // ajustable
		m_avatar = 0;
		m_superPower = null;
	}

	// place le player dans la case devant lui + libere case qu'il quitte
	public void setCoordFront(Entity e) {
		old_x = m_x;
		old_y = m_y;
		switch (m_direction.toString()) {
		case "N":
			m_modelGrid.m_grid[Math.floorMod(m_y - 1, ModelGrid.HEIGHT)][m_x] = this;
			m_y = Math.floorMod(m_y - 1, ModelGrid.HEIGHT);
			break;
		case "E":
			m_modelGrid.m_grid[m_y][Math.floorMod(m_x + 1, ModelGrid.WIDTH)] = this;
			m_x = Math.floorMod(m_x + 1, ModelGrid.HEIGHT);
			break;
		case "S":
			m_modelGrid.m_grid[Math.floorMod(m_y + 1, ModelGrid.HEIGHT)][m_x] = this;
			m_y = Math.floorMod(m_y + 1, ModelGrid.HEIGHT);
			break;
		case "W":
			m_modelGrid.m_grid[m_y][Math.floorMod(m_x - 1, ModelGrid.WIDTH)] = this; /////
			m_x = Math.floorMod(m_x - 1, ModelGrid.HEIGHT);
			break;
		default:
			throw new IllegalStateException();

		}
		m_modelGrid.freePos(old_x, old_y); ///// interval ou le joueur est sur 2
											///// cases
		Zombie.X_HIDER = m_x;
		Zombie.Y_HIDER = m_y;
		if (e != null)
			e.m_modelGrid.deadEntity.add(e);

	}

	// recree un obstacle iddentique a celui que le player vient de quitter
	// à la place qu'il vient de quitter
	public void setQuitObstacle(int avatar, int old_x, int old_y, Entity e) {
		setCoordFront(e);
		switch (avatar) {
		case 1:
			m_modelGrid.m_grid[old_y][old_x] = new Rabbit(m_modelGrid, m_modelGrid.m_autLapin, O, old_x, old_y);
			break;
		case 2:
			m_modelGrid.m_grid[old_y][old_x] = new Bush(m_modelGrid, m_modelGrid.m_autDecor, O, old_x, old_y);
			break;
		case 3:
			m_modelGrid.m_grid[old_y][old_x] = new Tree(m_modelGrid, m_modelGrid.m_autDecor, O, old_x, old_y);
			break;
		case 4:
			m_modelGrid.m_grid[old_y][old_x] = new Rock(m_modelGrid, m_modelGrid.m_autDecor, O, old_x, old_y);
			break;
		case 5:
			m_modelGrid.m_grid[old_y][old_x] = (m_modelGrid.m_game.m_isDay)
					? new ZombieDay(m_modelGrid, m_modelGrid.m_autZombie, T, old_x, old_y)
					: new ZombieNight(m_modelGrid, m_modelGrid.m_autZombie, T, old_x, old_y);
			break;
		default:
			throw new IllegalStateException();

		}
	}

	@Override
	public boolean Closest(Aut_Category c, Aut_Direction dir) {
		if (this.m_modelGrid.m_grid[m_y][m_x].m_category == c)
			return true;
		else
			return false;
	}

	// pop est commune aux deux joueurs donc on l'implémente ici
	// cette fonction sert à recuperer le pouvoir (le plus rapide)
	@Override
	public void Pop(Aut_Direction d) {
		recupPower();
	}

	@Override
	public void Wizz(Aut_Direction d) {
		// NOTHING
	}

}
