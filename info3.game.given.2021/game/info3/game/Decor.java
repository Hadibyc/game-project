package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public class Decor extends Entity {

	Decor(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_hp = 1;
	}

	@Override
	public void Pop(Aut_Direction d) {
		new PossessionAnimation(m_modelGrid.m_game, m_speed, this);
	}

	@Override
	public void Wizz(Aut_Direction d) {
		new PossessionAnimation(m_modelGrid.m_game, m_speed, this);
	}
}