/**
 * 
 */
package info3.game;

/**
 * @author chemsou
 *
 */
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

class GameMenu extends JFrame implements ChangeListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static JSlider slider;
	static JLabel label;
	JButton bouton1;
	JButton bouton2;
	JButton bouton3;
	Game game;
	JFrame frame;
	private JSlider nb_bots;
	private JSlider size;
	private JSlider nb_army;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	int[] setting;
	String[] automatSettings;
	private JFrame frame2;
	private JLabel pic;
	private JButton bouton4;
	private JFrame f;
	private JButton bouton5;

	GameMenu() {
		super();
		setting = new int[3];
		setting[0] = 40;
		setting[1] = 5;
		setting[2] = 5;
		automatSettings = new String[5];
		automatSettings[0] = "Zombie";
		automatSettings[1] = "J1";
		automatSettings[2] = "J2";
		automatSettings[3] = "Decor";
		automatSettings[4] = "Rabbit";
	}

	public void build() {
		frame = new JFrame("WereWolf Hunter");
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("resources/Werewolf Hunter.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pic = new JLabel(new ImageIcon(img));
		frame.getContentPane().add(pic);
		frame.setContentPane(pic);
		frame.setVisible(true);
		bouton1 = new JButton("PLAY");
		bouton2 = new JButton("SETTINGS");
		bouton3 = new JButton("EXIT");
		buttcreator(bouton1, 0);
		buttcreator(bouton2, 100);
		buttcreator(bouton3, 200);
		pic.add(bouton1);
		pic.add(bouton2);
		pic.add(bouton3);
		bouton1.addActionListener(this);
		bouton2.addActionListener(this);
		bouton3.addActionListener(this);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 800);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	// Si la valeur du slider est modifiée
	public void stateChanged(ChangeEvent e) {
		Object source = e.getSource();
		if (source.equals(size)) {
			label1.setText("La taille  du terrain sera  : " + size.getValue() + " x " + size.getValue());
			setting[0] = size.getValue();
		}
		if (source.equals(nb_army)) {
			label2.setText("Le nombre d'entité armée sera : " + nb_army.getValue());
			setting[1] = nb_army.getValue();
		}
		if (source.equals(nb_bots)) {
			label3.setText("Le nombre des bots générés sera  : " + nb_bots.getValue());
			setting[2] = (int) nb_bots.getValue() / 4;
		}

	}

	public static void buttcreator(JButton but, int h) {
		but.setBounds(350, 50 + h, 250, 50);
		but.setFont(new Font("Gill Sans MT", Font.BOLD, 14));
		but.setBackground(new Color(0x2dce98));
		but.setForeground(Color.white);
	}

	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source.equals(bouton3)) {
			System.exit(0);
		}
		if (source.equals(bouton1)) {
			frame.dispose();
			try {
				System.out.println("Game starting...");
				game = new Game(setting, automatSettings);
				System.out.println("Game started.");
			} catch (Throwable th) {
				th.printStackTrace(System.err);
			}
		}

		if (source.equals(bouton2)) {
			action();
		}
		if (source.equals(bouton4)) {
			f.dispose();
		}
		if (source.equals(bouton5)) {
			frame2.dispose();
		}

	}

	@SuppressWarnings("deprecation")
	public void action() {
		Settings set = new Settings();
		setting = set.getSettings();
		automatSettings = set.getAutomatSettings();

	}

	void setupslider(JSlider slider) {
		slider.setPaintTrack(true);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(20);
		slider.setMinorTickSpacing(20);
		slider.addChangeListener(this);

	}
}
