package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;

public class Tree extends Decor {

	static final int AVATAR = 3;

	public Tree(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_size = 2;
		m_speed = Delays.DECOR_DELAY;
		m_sprite = SpritesInstances.spTree; // TODO : Find a better sprite
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
	}

}
