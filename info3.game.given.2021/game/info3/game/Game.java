/*
 * Copyright (C) 2020  Pr. Olivier Gruber
 * Educational software for a basic game development
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: March, 2020
 *      Author: Pr. Olivier Gruber
 */
package info3.game;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import info3.game.graphics.GameCanvas;
import info3.game.sound.RandomFileInputStream;

public class Game {
	static Game game;

	public static void main(String args[]) throws Exception {
		GameMenu m = new GameMenu();
		m.build();
	}

	JFrame m_frame;
	JLabel m_text;
	GameCanvas m_canvas;
	CanvasListener m_listener;
	CanvasListener m_listener2;
	ModelGrid m_model;
	Hunter m_hunter;
	Sound m_music;
	ZombieDay[] m_humans;
	WereWolf m_wereWolf;
	Timers m_timers;
	int m_nbManches = 5;
	int m_nbManchesCourantes = 1;
	boolean done = true;
	boolean done2 = true;
	boolean m_isDay = true;

	int remainingTime; // Temps restant en secondes
	boolean m_isStarted;

	public static final int DAYTIME = 300; // in seconds

	class AutomatonListener implements TimerListener {
		@Override
		public void expired() {
			m_timers.setTimer(5, this);
			for (Entity ent : m_model.automatedEntity) {
				ent.step();

			}
			m_model.automatedEntity.removeAll(m_model.deadEntity);
			m_model.deadEntity.clear();
			m_model.automatedEntity.addAll(m_model.addEntity);
			m_model.addEntity.clear();
			m_model.preDisplaying(m_model.hunterx, m_model.huntery, 1);
			m_model.preDisplaying(m_model.hiderx, m_model.hidery, 0);

		}
	}

	class DaytimeListener implements TimerListener {
		@Override
		public void expired() {
			m_timers.setTimer(DAYTIME * 1000, this);
			for (Entity ent : m_model.automatedEntity) {
				ent.invertRole();
			}
			remainingTime = DAYTIME;
			m_listener.getPlayers();
			m_isDay = !m_isDay;
		}
	}

	class RemainingListener implements TimerListener {
		@Override
		public void expired() {
			m_timers.setTimer(1000, this);
			remainingTime--;
		}
	}

	class BeginGameListener implements TimerListener {
		@Override
		public void expired() {
			m_timers.setTimer(DAYTIME * 1000, new DaytimeListener());
			m_timers.setTimer(1000, new RemainingListener());
			m_model.m_hunter.m_blocked = false;
			m_model.m_hider.m_showen = true;
			m_isStarted = true;
		}
	}

	class PowerListener implements TimerListener {

		@Override
		public void expired() {
			m_timers.setTimer(DAYTIME * 1000 / (2 + (int) (Math.random() * ((4 - 2) + 1))), new PowerListener());
			PowerAvailable();
		}

	}

	int[] réglage = new int[3];

	private JLabel m_barre;
	private JPanel m_p;
	JProgressBar proj1;
	JProgressBar proj2;

	public Game(int[] reg, String[] automatSettings) throws Exception {
		// creating a cowboy, that would be a model
		// in an Model-View-Controller pattern (MVC)

		// creating a listener for all the events
		// from the game canvas, that would be
		// the controller in the MVC pattern

		// Load all sprites
		SpritesInstances.loadSprites();
		Images.loadImages();

		m_listener = new CanvasListener(this);
		// creating the game canvas to render the game,
		// that would be a part of the view in the MVC pattern
		m_canvas = new GameCanvas(m_listener);
		m_timers = new Timers();

		System.out.println("  - creating frame...");
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

		m_frame = m_canvas.createFrame(d);
		m_model = new ModelGrid(m_canvas.getWidth(), m_canvas.getHeight(), reg, automatSettings, this);
		m_listener.getPlayers();
		System.out.println("  - setting up the frame...");
		setupFrame();
		m_timers.setTimer(5, new AutomatonListener());
		remainingTime = DAYTIME;
		m_timers.setTimer(15 * 1000, new BeginGameListener());
		m_timers.setTimer(DAYTIME * 1000 / (2 + (int) (Math.random() * ((4 - 2) + 1))), new PowerListener());

	}

	/*
	 * Then it lays out the frame, with a border layout, adding a label to the north
	 * and the game canvas to the center.
	 */
	private void setupFrame() {

		m_frame.setTitle("Game");
		m_frame.setLayout(new BorderLayout());

		m_text = new JLabel();
		m_text.setText("Tick: 0ms FPS=0");
		m_frame.add(m_text, BorderLayout.NORTH);

		// center the m_frame on the screen
		m_frame.setLocationRelativeTo(null);

		// make the vindow visible
		m_frame.add(m_canvas, BorderLayout.CENTER);
		m_frame.setVisible(true);
		m_p = new JPanel();
		m_frame.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLUE));
		proj2 = new JProgressBar();
		proj2.setValue(100);
		m_p.add(proj2);

		m_barre = new JLabel();

		m_p.add(m_barre);
		proj1 = new JProgressBar();
		proj1.setValue(100);
		m_p.add(proj1);
		m_frame.add(m_p, BorderLayout.SOUTH);

	}

	/*
	 * ================================================================ All the
	 * methods below are invoked from the GameCanvas listener, once the m_frame is
	 * visible on the screen.
	 * ==============================================================
	 */

	/*
	 * Called from the GameCanvas listener when the frame
	 */
	String m_musicName;

	void loadMusic() {
		m_musicName = m_musicNames[m_musicIndex];
		String filename = "resources/" + m_musicName + ".ogg";
		m_musicIndex = (m_musicIndex + 1) % m_musicNames.length;
		try {
			RandomAccessFile file = new RandomAccessFile(filename, "r");
			RandomFileInputStream fis = new RandomFileInputStream(file);
			m_canvas.playMusic(fis, 0, 1.0F);
		} catch (Throwable th) {
			th.printStackTrace(System.err);
			System.exit(-1);
		}
	}

	private int m_musicIndex = 0;
	private String[] m_musicNames = new String[] { "Runaway-Food-Truck" };

	private long m_textElapsed;
	boolean GameOver = true;;

	/*
	 * This method is invoked almost periodically, given the number of milli-seconds
	 * that elapsed since the last time this method was invoked.
	 */
	void tick(long elapsed, int code, int codePlayer1, int codePlayer2) {

		code = m_listener.m_code_walk;
		if (m_model.m_hider != null) {
			m_model.hiderx = m_model.m_hider.getX();
			m_model.hidery = m_model.m_hider.getY();
		}
		if (m_model.m_hunter != null) {
			m_model.hunterx = m_model.m_hunter.getX();
			m_model.huntery = m_model.m_hunter.getY();
		}
		m_textElapsed += elapsed;
		if (m_textElapsed > 1000) {
			m_textElapsed = 0;
			float period = m_canvas.getTickPeriod();
			int fps = m_canvas.getFPS();

			String txt = "Tick=" + period + "ms";
			while (txt.length() < 15)
				txt += " ";
			txt = txt + fps + " fps   ";
			m_text.setText(txt);

		}

		if (m_nbManches >= 0) {
			if (remainingTime == 1 && done) {
				m_nbManches--;
				m_nbManchesCourantes++;
				done = false;
			}
			if (remainingTime == 2) {
				done = true;
			}
			if (m_model.m_gamePower.getAvailable()) {
				PowerAvailable();
			} else {
				PowerUnavailable();
			}
		}
		if (m_nbManches < 0) {
			if (done2) {
				GameOver fin = new GameOver(0, this);
				m_frame.setVisible(false);
				fin.reply();
				done2 = false;
			}
		}

	}

	private void PowerAvailable() {

		m_model.m_gamePower.setAvailable(true);
		m_barre.setText(" il y a un pouvoir disponible  , Temps restant jusqu'à la fin de la manche :" + remainingTime
				+ "   manche num : " + m_nbManchesCourantes + "       manches restantes :" + (m_nbManches) + " ");
		// changer couleur texte
	}

	private void PowerUnavailable() {

		m_model.m_gamePower.setAvailable(false);
		m_barre.setText("Il n'y a pas un pouvoir disponible , Temps restant jusqu'à la fin de la manche :"
				+ remainingTime + "   manche num : " + m_nbManchesCourantes + "       manches restantes :"
				+ (m_nbManches) + " ");

		// remettre couleur texte à la normale

	}

	/*
	 * This request is to paint the Game Canvas, using the given graphics. This is
	 * called from the GameCanvasListener, called from the GameCanvas.
	 */
	public void paint(Graphics g) {
		// get the size of the canvas
		int width = m_canvas.getWidth();
		int height = m_canvas.getHeight();
		// erase background
		g.setColor(Color.black);
		g.fillRect(0, 0, width, height);

		m_model.paint(g, width, height);

		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(40));
		g2.setColor(Color.BLACK.darker());
		Double line = new Line2D.Double(width / 2, 0, width / 2, height);
		g2.draw(line);
	}

	public Timers getTimers() {
		return m_timers;
	}

	public void stop() {
		m_timers.m_timer.stop();
	}
}
