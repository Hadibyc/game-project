package info3.game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.Timer;

public class Timers {

	Timer m_timer;

	public Timers() {
		m_timer = new Timer(-1, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				expired();
			}
		});
	}

	public void setTimer(int delay, TimerListener listener) {
		updateDelays();
		if (delay >= 0) {
			addTimer(delay, listener);
		} else {
			removeTimer(listener);
		}
	}

	private LinkedList<Object> m_pending_requests = new LinkedList<Object>();
	private LinkedList<Object> m_expiring_requests = new LinkedList<Object>();
	private long m_last;

	public void expired() {
		updateDelays();
		LinkedList<Object> toRemove = new LinkedList<Object>();
		Iterator<Object> iter = m_pending_requests.iterator();
		int min_delay = -99999, delay;
		while (iter.hasNext()) {
			delay = (int) iter.next();
			if (delay > 0 && (min_delay == -99999 || delay < min_delay))
				min_delay = delay;
			if (delay <= 0) {
				toRemove.add(delay);
				Object expiringRequest = iter.next();
				toRemove.add(expiringRequest);
				m_expiring_requests.add(0, expiringRequest);
			} else {
				try {
					iter.next();
				} catch (Exception e) {
					throw new IllegalStateException("Toolkit : m_pending_requests list must have an even length");
				}
			}
		}
		m_pending_requests.removeAll(toRemove);
		if (min_delay > 0) {
			m_timer.stop();
			m_timer.setDelay(min_delay);
			m_timer.start();
		}

		Iterator<Object> iter_exp = m_expiring_requests.iterator();
		TimerListener tl;
		while (iter_exp.hasNext()) {
			tl = (TimerListener) iter_exp.next();
			tl.expired();
		}
		m_expiring_requests = new LinkedList<Object>();
	}

	private void addTimer(int delay, TimerListener listener) {
		int d, min_delay = 0;
		int ind_to_update = -1;
		int i = 0;
		TimerListener tl;
		Iterator<Object> iter = m_pending_requests.iterator();
		while (iter.hasNext()) {
			d = (int) iter.next();
			if (i == 0 || d < min_delay) {
				min_delay = d;
			}
			try {
				tl = (TimerListener) iter.next();
				if (tl == listener) {
					ind_to_update = i;
				}
			} catch (Exception e) {
				throw new IllegalStateException("Toolkit : m_pending_requests list must have an even length");
			}

			i += 2;
		}
		if (delay < min_delay || m_pending_requests.size() == 0) {
			m_timer.stop();
			m_timer.setDelay(delay);
			m_timer.start();
		}
		if (ind_to_update != -1) {
			m_pending_requests.set(ind_to_update, delay);
		} else {
			m_pending_requests.add(delay);
			m_pending_requests.add(listener);
		}
	}

	private void updateDelays() {
		long now = System.currentTimeMillis();
		int delta = (int) (now - m_last);
		int old_delay, new_delay;
		int index = 0;
		m_last = now;
		Iterator<Object> iter = m_pending_requests.iterator();
		while (iter.hasNext()) {
			old_delay = (int) iter.next();
			new_delay = old_delay - delta;
			m_pending_requests.set(index, new_delay);
			try {
				iter.next();
			} catch (Exception e) {
				throw new IllegalStateException("Toolkit : m_pending_requests list must have an even length");
			}
			index += 2;
		}
	}

	private void removeTimer(TimerListener listener) {
		int i = 0;
		Iterator<Object> iter = m_pending_requests.iterator();
		TimerListener tl;
		while (iter.hasNext()) {
			iter.next();
			try {
				tl = (TimerListener) iter.next();
				if (tl == listener) {
					m_pending_requests.remove(i);
					m_pending_requests.remove(i);
					return;
				}
			} catch (Exception e) {
				throw new IllegalStateException("Toolkit : m_pending_requests list must have an even length");
			}
			i += 2;
		}
	}
}
