/*
 * Copyright (C) 2020  Pr. Olivier Gruber
 * Educational software for a basic game development
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: March, 2020
 *      Author: Pr. Olivier Gruber
 */
package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

/**
 * A simple class that holds the images of a sprite for an animated cowbow.
 *
 */

public class Hunter extends Player1 {

	int m_nb_trap;

	public Hunter(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.HUNTER_DELAY;// ajustable
		m_damage = 10; // ajustable
		m_sprite = SpritesInstances.spHuman;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
		m_hitAnimation = new HitAnimation(m_modelGrid.m_game, m_speed, this);
		m_blocked = !m_modelGrid.m_game.m_isStarted;
		m_nb_trap = 3;
	}

	public void tick(long Elapsed, int Code) {
	}

	// La fonction permettant au chasseur de frapper l'entité devant lui
	@Override
	public void Hit(Aut_Direction dir) {
		if (!m_isMoving && !m_blocked) {
			m_hitAnimation.start();
			if (Cell(FRONT, A)) {
				Entity toHit = getEntity(FRONT);
				Hurt(this, toHit);
				if (toHit.m_hp <= 0) {
					if (toHit instanceof Player) {
						toHit.m_dieAnimation = new DieAnimation(m_modelGrid.m_game, m_speed, toHit);
						toHit.setM_isDead();
					} else {
						toHit.m_category = V;
						m_game.m_model.freePos(toHit.m_x, toHit.m_y);
						m_game.m_model.deadEntity.add(toHit);
					}
				}

			} else if (Cell(FRONT, V)) {
				return;
			} else {
				Entity toHit = getEntity(FRONT);
				Hurt(this, toHit);
				Hurt(this, this);
				if (this.m_hp <= 0) {
					if (this instanceof Player) {
						this.m_dieAnimation = new DieAnimation(m_modelGrid.m_game, m_speed, this);
						this.setM_isDead();
					} else {
						this.m_category = V;
						m_game.m_model.freePos(this.m_x, this.m_y);
						m_game.m_model.deadEntity.add(this);
					}
				}
				if (toHit.m_hp <= 0) {
					toHit.m_category = V;
					m_game.m_model.freePos(toHit.m_x, toHit.m_y);
					m_game.m_model.deadEntity.add(toHit);

				}
			}
		}
	}

	@Override
	public void Wizz(Aut_Direction d) {
		Entity e = getEntity(FRONT);
		if (Cell(m_direction, D)) {
			m_nb_trap++;
			m_modelGrid.freePos(e.m_x, e.m_y);
		} else if (Cell(m_direction, V) && m_nb_trap > 0) {
			m_nb_trap--;
			m_modelGrid.m_grid[e.m_y][e.m_x] = new Trap(m_modelGrid, m_modelGrid.m_autTrap, D, e.m_x, e.m_y);
		}

	}

	@Override
	public void invertRole() {
		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
		if (m_isMoving) {
			m_modelGrid.freePos(old_x, old_y);
			m_modelGrid.freePos(m_x, m_y);
		}
		this.m_blocked = true;
		Prey nightForm = new Prey(m_modelGrid, m_automaton, A, m_x, m_y);
		nightForm.m_game = m_modelGrid.m_game;
		nightForm.setHp(m_hp);
		nightForm.m_direction = m_direction;
		nightForm.m_movement.changeDirection();
		nightForm.m_keyWalk = this.m_keyWalk;
		nightForm.m_keyAction = this.m_keyAction;
		m_modelGrid.m_hunter = nightForm;
		m_modelGrid.m_p1 = nightForm;
		if (m_modelGrid.automatedSettings[1].equals("J1")) {
			m_modelGrid.automatPlayer1.remove(this);
			m_modelGrid.automatPlayer1.add(nightForm);
		} else if (m_modelGrid.automatedSettings[1].equals("J2")) {
			m_modelGrid.automatPlayer2.remove(this);
			m_modelGrid.automatPlayer2.add(nightForm);
		}
	}
}
