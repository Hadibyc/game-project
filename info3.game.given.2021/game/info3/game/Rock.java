package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;

public class Rock extends Decor {

	static final int AVATAR = 4;

	public Rock(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.DECOR_DELAY;
		m_sprite = SpritesInstances.spRock; // TODO : Find a better sprite
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
	}
}
