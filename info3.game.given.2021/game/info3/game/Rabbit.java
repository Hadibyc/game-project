package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public class Rabbit extends Entity {
	static final int AVATAR = 1;

	public Rabbit(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		this.m_hp = 10;
		this.m_speed = Delays.RABBIT_DELAY;
		m_sprite = SpritesInstances.spRabbit;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
	}

	@Override
	public void Pop(Aut_Direction d) {
		Turn(RIGHT);
	}

	@Override
	public void Wizz(Aut_Direction d) {
		Wait();
	}

}
