package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.Game.*;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;
import info3.game.automaton.builder.Aut_Key;
import info3.game.automaton.builder.Aut_State;

abstract public class Entity {
	public ModelGrid m_modelGrid;
	Aut_Automaton m_automaton;
	Aut_State m_currentState;
	int m_imageIndex;
	Aut_Direction m_walkDirection;
	long m_imageElapsed;
	long m_moveElapsed;
	int m_width;
	int m_height;
	int m_spritew;
	int m_spriteh;
	public boolean m_blocked;
	BufferedImage[] m_imagesHandler;
	BufferedImage[] m_images;
	int m_x, m_y;
	int old_x, old_y;
	int m_ncolsSprite = 13;
	int m_nrowsSprite = 21;
	public int m_size;
	public int m_speed;
	public Aut_Direction m_direction;
	public long m_elapsed;
	public Aut_Category m_category;
	public int m_hp;
	public boolean m_isDead;
	public Aut_Key m_keyWalk;
	public Aut_Key m_keyAction;
	Movement m_movement;
	HitAnimation m_hitAnimation;
	Sprite m_sprite;
	boolean m_isMoving;
	boolean m_isHitting;
	boolean m_isWaiting;
	boolean m_isPossessing = false;
	PossessionAnimation m_possessionAnimation;
	DieAnimation m_dieAnimation;
	int m_detection = 4;
	public int m_damage = 5;

	public static final Aut_Direction EAST = new Aut_Direction("E");
	public static final Aut_Direction NORTH = new Aut_Direction("N");
	public static final Aut_Direction SOUTH = new Aut_Direction("S");
	public static final Aut_Direction WEST = new Aut_Direction("W");
	public static final Aut_Direction FRONT = new Aut_Direction("F");
	public static final Aut_Direction BACK = new Aut_Direction("B");
	public static final Aut_Direction LEFT = new Aut_Direction("L");
	public static final Aut_Direction RIGHT = new Aut_Direction("R");

	public static final Aut_Category V = new Aut_Category("V"); // vide
	public static final Aut_Category O = new Aut_Category("O"); // obstacles (buisson, rocher, lapins...
	public static final Aut_Category A = new Aut_Category("A"); // le traqué
	public static final Aut_Category T = new Aut_Category("T"); // zombie + chasseur
	public static final Aut_Category G = new Aut_Category("G"); // case réservée
	public static final Aut_Category D = new Aut_Category("D"); // trap
	public static final Aut_Category UNDERSCORE = new Aut_Category("_");

	Aut_Direction oldir = EAST;
	public Game m_game;
	public boolean m_showen;
	boolean m_isOnTrap = false;

	public Entity(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {

		m_modelGrid = model;
		m_x = x;
		m_y = y;
		m_category = c;
		m_modelGrid.setEntity(this);
		m_automaton = a;
		if (a != null)
			m_currentState = a.getInitialState();
		m_direction = EAST;
		m_blocked = false;
		m_showen = true;
		m_detection = 4;
		if (!c.equals(V))
			m_modelGrid.addEntity.add(this);

	}

	public static BufferedImage[] loadSprite(String filename, int nrows, int ncols) {
		File imageFile = new File(filename);
		if (imageFile.exists()) {
			BufferedImage image = null;
			try {
				image = ImageIO.read(imageFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int width = image.getWidth(null) / ncols;
			int height = image.getHeight(null) / nrows;

			BufferedImage[] images = new BufferedImage[nrows * ncols];
			for (int i = 0; i < nrows; i++) {
				for (int j = 0; j < ncols; j++) {
					int x = j * width;
					int y = i * height;
					images[(i * ncols) + j] = image.getSubimage(x, y, width, height);
				}
			}
			return images;
		}
		return null;
	}

	public int distance(Entity e1, int x, int y) {
		return (int) Math.abs(Math.sqrt((double) Math.pow(x - e1.getX(), 2.0) + Math.pow(y - e1.getY(), 2.0))) + 1;
	}

	public int distance2(Entity e1, int x, int y) {
		return Math.abs((int) Math.sqrt((double) Math.pow(x - e1.getX(), 2.0) + Math.pow(y - e1.getY(), 2.0)));
	}

	public void paint(Graphics g, int width, int height, int i, int j) {
		if (m_isDead)
			m_dieAnimation.paint(g, width, height, i, j);
		else if (m_isHitting) {
			m_hitAnimation.paint(g, width, height, i, j);
		}

		else if (m_isPossessing)
			m_possessionAnimation.paint(g, width, height, i, j);
		else
			m_movement.paint(g, width, height, i, j);
	}

	public boolean Closest(Aut_Category c, Aut_Direction d) {

		int closest_x = this.m_modelGrid.hiderx;
		int closest_y = this.m_modelGrid.hidery;
		int d_min = distance2(this, this.m_modelGrid.hiderx, this.m_modelGrid.hidery);
		Aut_Direction dir;

		// calcul du point le plus proche

		int dist = distance2(this, this.m_modelGrid.hiderx - ModelGrid.WIDTH, this.m_modelGrid.hidery);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx - ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery;
		}
		dist = distance2(this, this.m_modelGrid.hiderx, this.m_modelGrid.hidery - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx;
			closest_y = this.m_modelGrid.hidery - ModelGrid.HEIGHT;
		}
		dist = distance2(this, this.m_modelGrid.hiderx - ModelGrid.WIDTH, this.m_modelGrid.hidery - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx - ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery - ModelGrid.HEIGHT;
		}
		dist = distance2(this, this.m_modelGrid.hiderx + ModelGrid.WIDTH, this.m_modelGrid.hidery);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx + ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery;
		}

		dist = distance2(this, this.m_modelGrid.hiderx, this.m_modelGrid.hidery + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx;
			closest_y = this.m_modelGrid.hidery + ModelGrid.HEIGHT;
		}
		dist = distance2(this, this.m_modelGrid.hiderx + ModelGrid.WIDTH, this.m_modelGrid.hidery - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx + ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery - ModelGrid.HEIGHT;
		}
		dist = distance2(this, this.m_modelGrid.hiderx - ModelGrid.WIDTH, this.m_modelGrid.hidery + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx - ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery + ModelGrid.HEIGHT;
		}
		dist = distance2(this, this.m_modelGrid.hiderx + ModelGrid.WIDTH, this.m_modelGrid.hidery + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = this.m_modelGrid.hiderx + ModelGrid.WIDTH;
			closest_y = this.m_modelGrid.hidery + ModelGrid.HEIGHT;
		}

		// test si il est dans le rayon de detection

		if (d_min > m_detection) {
			m_detection = 4;
			return false;
		}

		// calcul de la direction à laquelle il est le plus proche de this

		int d_min1;
		dist = distance2(this, closest_x + m_detection, closest_y);
		d_min1 = dist;
		dir = WEST;

		dist = distance2(this, closest_x - m_detection, closest_y);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = EAST;
		}
		dist = distance2(this, closest_x, closest_y + m_detection);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = NORTH;
		}
		dist = distance2(this, closest_x, closest_y - m_detection);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = SOUTH;
		}

		// test si c'est la bonne direction

		if (dir.equals(d)) {
			m_detection = 6;
			return true;
		}

		return false;
	}

	public void Hurt(Entity player, Entity entity) {
		entity.m_hp -= player.m_damage;

		if (entity instanceof Player1)
			m_game.proj1.setValue(entity.m_hp);
		else if (entity instanceof Player2)
			m_game.proj2.setValue(entity.m_hp);
	}

	public boolean isM_isDead() {
		return m_isDead;
	}

	public void setM_isDead() {
		m_isDead = true;
		Zombie.X_HIDER = 90000;
		Zombie.Y_HIDER = 90000;
	}

	public Aut_State getCurrentState() {
		return m_currentState;
	}

	public void setCurentState(Aut_State state) {
		m_currentState = state;
	}

	public Entity[][] getGrid() {
		return m_modelGrid.m_grid;
	}

	public int getX() {
		return m_x;
	}

	public void setX(int m_x) {
		this.m_x = m_x;
	}

	public int getY() {
		return m_y;
	}

	public void setY(int m_y) {
		this.m_y = m_y;
	}

	public int getSize() {
		return m_size;
	}

	public void setSize(int m_size) {
		this.m_size = m_size;
	}

	public int getSpeed() {
		return m_speed;
	}

	public void setSpeed(int m_speed) {
		this.m_speed = m_speed;
	}

	public int getHp() {
		return m_hp;
	}

	public void setHp(int m_hp) {
		this.m_hp = m_hp;
	}

	public void step() {
		if (m_automaton != null)
			m_automaton.step(this);
	}

	public void Move(Aut_Direction dir) {

		if ((!m_isMoving) && (!m_isWaiting)) {
			if (!Cell(this.relativDirToAbsolute(dir), V) || m_blocked) {
				Turn(dir);
			} else {
				old_x = m_x;
				old_y = m_y;
				Turn(dir);
				m_movement.start();
				switch (dir.toString()) {
				case "H":
					break;
				case "N":
					m_modelGrid.m_grid[Math.floorMod(m_y - 1, ModelGrid.HEIGHT)][m_x].m_category = G; // Reservation
					// de
					// la
					// case

					m_y = Math.floorMod(m_y - 1, ModelGrid.HEIGHT);
					break;
				case "S":
					m_modelGrid.m_grid[Math.floorMod(m_y + 1, ModelGrid.HEIGHT)][m_x].m_category = G;

					m_y = Math.floorMod(m_y + 1, ModelGrid.HEIGHT);
					break;
				case "E":
					m_modelGrid.m_grid[m_y][Math.floorMod(m_x + 1, ModelGrid.WIDTH)].m_category = G;

					m_x = Math.floorMod(m_x + 1, ModelGrid.WIDTH);
					break;
				case "W":
					m_modelGrid.m_grid[m_y][Math.floorMod(m_x - 1, ModelGrid.WIDTH)].m_category = G;
					m_x = Math.floorMod(m_x - 1, ModelGrid.WIDTH);
					break;
				default:
					throw new IllegalStateException();
				}
				// m_modelGrid.freePos(old_x, old_y);
			}

		}
		if (m_category.equals(A)) {
			Zombie.X_HIDER = m_x;
			Zombie.Y_HIDER = m_y;
		}
	}

	public void Turn(Aut_Direction d) {
		if (!m_isMoving && !m_isWaiting) {
			switch (d.toString()) {
			case "H":
				break;
			case "N":
			case "S":
			case "E":
			case "W":
				m_direction = d;
				break;
			case "F":
				break;
			case "B":
				switch (m_direction.toString()) {
				case "N":
					m_direction = SOUTH;
					break;
				case "S":
					m_direction = NORTH;
					break;
				case "E":
					m_direction = WEST;
					break;
				case "W":
					m_direction = EAST;
					break;
				default:
					throw new IllegalStateException();
				}
				break;
			case "R":
				switch (m_direction.toString()) {
				case "N":
					m_direction = EAST;
					break;
				case "S":
					m_direction = WEST;
					break;
				case "E":
					m_direction = SOUTH;
					break;
				case "W":
					m_direction = NORTH;
					break;
				default:
					throw new IllegalStateException();
				}
				break;
			case "L":
				switch (m_direction.toString()) {
				case "N":
					m_direction = WEST;
					break;
				case "S":
					m_direction = EAST;
					break;
				case "E":
					m_direction = NORTH;
					break;
				case "W":
					m_direction = SOUTH;
					break;
				default:
					throw new IllegalStateException();
				}
				break;
			}
			// Sprite_Direction(m_direction, 8);
			m_walkDirection = m_direction;
			if (!m_category.equals(D) && !(m_movement == null))
				m_movement.changeDirection();

		}
	}

	// cette fonction prend une direction et renvoie son absolue
	// si son argument est une direction relative
	public Aut_Direction relativDirToAbsolute(Aut_Direction d) {
		switch (d.toString()) {
		case "H":
		case "N":
		case "E":
		case "S":
		case "W":
			return d;
		case "F":
			switch (m_direction.toString()) {
			case "N":
				return NORTH;
			case "E":
				return EAST;
			case "S":
				return SOUTH;
			case "W":
				return WEST;
			default:
				throw new IllegalStateException();
			}
		case "R":
			switch (m_direction.toString()) {
			case "N":
				return EAST;
			case "E":
				return SOUTH;
			case "S":
				return WEST;
			case "W":
				return NORTH;
			default:
				throw new IllegalStateException();
			}
		case "B":
			switch (m_direction.toString()) {
			case "N":
				return SOUTH;
			case "E":
				return WEST;
			case "S":
				return NORTH;
			case "W":
				return EAST;
			default:
				throw new IllegalStateException();
			}
		case "L":
			switch (m_direction.toString()) {
			case "N":
				return WEST;
			case "E":
				return NORTH;
			case "S":
				return EAST;
			case "W":
				return SOUTH;
			default:
				throw new IllegalStateException();
			}
		default:
			throw new IllegalStateException();
		}
	}

	// avec la direction absolue on trouve la case avec X et Y
	// et on compare avec la categorie qu'on cherche
	public boolean Cell(Aut_Direction d, Aut_Category c) {
		Aut_Category cat = getEntity(d).m_category;
		if (cat.equals(c)) {
			return true;
		} else
			return false;
	}

	public void Hit(Aut_Direction choice, int ChosenLine) {
		Turn(choice);
		m_images = new BufferedImage[6];
		switch (choice.toString()) {
		case "N":
			for (int i = 0; i <= 5; i++) {
				m_images[i] = this.m_imagesHandler[13 * ChosenLine + i];
			}
			break;
		case "W":
			for (int i = 0; i <= 5; i++) {
				m_images[i] = this.m_imagesHandler[13 * (ChosenLine + 1) + i];
			}
			break;
		case "S":
			for (int i = 5; i <= 5; i++) {
				m_images[i] = this.m_imagesHandler[13 * (ChosenLine + 2) + i];
			}
			break;

		case "E":
			for (int i = 0; i <= 5; i++) {
				m_images[i] = this.m_imagesHandler[13 * (ChosenLine + 3) + i];
			}
			break;

		default:
			break;
		}
	}

	public Entity getEntity(Aut_Direction dir) {
		Entity e;
		Aut_Direction absoluteD = relativDirToAbsolute(dir);
		switch (absoluteD.toString()) {
		case "H":
			e = this;
			break;
		case "N":
			e = m_modelGrid.m_grid[Math.floorMod(m_y - 1, ModelGrid.HEIGHT)][m_x];
			break;
		case "E":
			e = m_modelGrid.m_grid[m_y][Math.floorMod(m_x + 1, ModelGrid.WIDTH)];
			break;
		case "S":
			e = m_modelGrid.m_grid[Math.floorMod(m_y + 1, ModelGrid.HEIGHT)][m_x];
			break;
		case "W":
			e = m_modelGrid.m_grid[m_y][Math.floorMod(m_x - 1, ModelGrid.WIDTH)];
			break;
		default:
			throw new IllegalStateException();
		}
		return e;
	}

	public void Egg(Aut_Direction m_dir) {
		throw new RuntimeException("Not yet implemented");
	}

	public void Explode() {
		throw new RuntimeException("Not yet implemented");

	}

	public void Get() {
		throw new RuntimeException("Not yet implemented");
	}

	public boolean GotPower() {
		throw new RuntimeException("Not yet implemented");
		// return false;
	}

	public boolean GotStuff() {
		throw new RuntimeException("Not yet implemented");
		// return false;
	}

	public void Hit(Aut_Direction dir) {
		if (!m_isMoving) {
			Turn(dir);
			if (m_hitAnimation != null) {
				m_hitAnimation.updateSprite(this);
				m_hitAnimation.start();
			}
			if (!(Cell(FRONT, V))) {
				Entity toHit = getEntity(FRONT);
				Hurt(this, toHit);
				if (toHit.m_hp <= 0) {
					if (toHit instanceof Player) {
						toHit.m_dieAnimation = new DieAnimation(m_modelGrid.m_game, m_speed, toHit);
						toHit.setM_isDead();
					} else {
						toHit.m_category = V;
						m_game.m_model.freePos(toHit.m_x, toHit.m_y);
						m_game.m_model.deadEntity.add(toHit);
					}

					// m_modelGrid.freePos(toHit.m_x, toHit.m_y);
				}

			}
		}
	}

	public void Jump(Aut_Direction m_dir) {
		throw new RuntimeException("Not yet implemented");
	}

	public boolean Key(Aut_Key k) {
		boolean walk, action;
		if (m_keyWalk == null)
			walk = false;
		else
			walk = m_keyWalk.equals(k);
		if (m_keyAction == null)
			action = false;
		else
			action = m_keyAction.equals(k);

		return walk || action;
	}

	public boolean MyDir(Aut_Direction m_dir) {
		if (m_dir.equals(m_direction))
			return true;
		return false;
	}

	public void Pick(Aut_Direction m_dir) {
		throw new RuntimeException("Not yet implemented");
	}

	public void Power() {
	}

	public void Pop(Aut_Direction m_dir) {
	}

	public void Protect(Aut_Direction m_dir) {
		throw new RuntimeException("Not yet implemented");
	}

	public void Store() {
		throw new RuntimeException("Not yet implemented");
	}

	public void Throw(Aut_Direction m_dir) {
		throw new RuntimeException("Not yet implemented");
	}

	public void Wait() {
		if (!m_isMoving) {
			m_isWaiting = !m_isWaiting;
		}
	}

	public void Wizz(Aut_Direction m_dir) {
	}

	public void setKeyWalk(Aut_Key key) {
		m_keyWalk = key;
	}

	public void clearKeyWalk() {
		m_keyWalk = null;
	}

	public void setKeyAction(Aut_Key key) {
		m_keyAction = key;
	}

	public void clearKeyAction() {
		m_keyAction = null;
	}

	public int getAvatarNumber() {
		return -1;
	}

	public void invertRole() {
	}

	protected void tick(long elapsed, int codePlayer1) {

	}

}
