/**
 * 
 */
package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import info3.game.Animation.AnimationListener;

/**
 * @author chemsou
 *
 */
public class DieAnimation extends Animation {
	private int m_maxIdx;
	private int m_spriteLine = 20;
	private int m_spriteWidth = 13;
	private int m_nbImg = 6;
	Game m_game;

	public DieAnimation(Game g, int delay, Entity e) {
		super(g, delay, null);
		m_game = g;
		m_listener = new DieListener();
		m_entity = e;
		m_sprite = e.m_sprite;
		m_idx = m_spriteWidth * m_spriteLine;
		m_maxIdx = m_spriteWidth * m_spriteLine + m_nbImg;
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
		setPosition(m_entity.m_x, m_entity.m_y, 2);
		// TODO Auto-generated constructor stub
		super.start();

	}

	public class DieListener implements AnimationListener {
		@Override
		public void done(Animation a) {

			m_entity.m_category = m_entity.V;
			m_game.m_model.freePos(m_entity.m_x, m_entity.m_y);
			m_game.m_model.deadEntity.add(m_entity);

			if (m_game.GameOver) {
				if (m_game.proj1.getValue() == 0) {
					GameOver fin = new GameOver(1, m_game);
					m_game.m_frame.setVisible(false);
					fin.reply();
					m_game.GameOver = false;

				}
				if (m_game.proj2.getValue() == 0) {
					GameOver fin = new GameOver(2, m_game);
					m_game.m_frame.setVisible(false);
					fin.reply();
					m_game.GameOver = false;
				}

			}
		}
	}

	@Override
	public boolean done() {
		return m_done;
	}

	@Override
	protected boolean nextImage() {
		m_idx++;
		if (m_idx < m_maxIdx) {
			m_img = (BufferedImage) m_sprite.m_images[m_idx];
			return true;
		} else {
			m_done = true;
			m_listener.done(this);
			return false;
		}
	}

	@Override
	protected void paint(Graphics g, int width, int height, int i, int j) {
		g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * i),
				((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * j), m_scale * m_img.getWidth(),
				m_scale * m_img.getHeight(), null);
	}

	@Override
	public void setPosition(int x, int y, int scale) {
		// TODO Auto-generated method stub
		m_x = x;
		m_y = y;
		m_scale = scale;

	}

}
