package info3.game;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SpritesInstances {
	public static Sprite spBush;
	public static Sprite spGrass;
	public static Sprite spHider;
	public static Sprite spHuman;
	public static Sprite spPrey;
	public static Sprite spRabbit;
	public static Sprite spRock;
	public static Sprite spTree;
	public static Sprite spWerewolf;
	public static Sprite spZombieDay;
	public static Sprite spZombieDayAngry;
	public static Sprite spZombieNight;
	public static Sprite spZombieNightAngry;
	public static Sprite spTrap;
	public static Sprite spStar;

	public static void loadSprites() {
		try {
			spBush = new Sprite(new FileInputStream("resources//BushSprite.png"), 21, 13);
			spGrass = new Sprite(new FileInputStream("resources//grass.png"), 1, 1);
			spHider = new Sprite(new FileInputStream("resources//Hider.png"), 21, 13);
			spHuman = new Sprite(new FileInputStream("resources//Humans.png"), 21, 13);
			spPrey = new Sprite(new FileInputStream("resources//Prey.png"), 21, 13);
			spRabbit = new Sprite(new FileInputStream("resources//rabbit.png"), 21, 13);
			spRock = new Sprite(new FileInputStream("resources//RockSprite.png"), 21, 13);
			spTree = new Sprite(new FileInputStream("resources//TreeSprite.png"), 21, 13);
			spWerewolf = new Sprite(new FileInputStream("resources//WereWolf.png"), 21, 13);
			spZombieDay = new Sprite(new FileInputStream("resources//ZombieDay.png"), 21, 13);
			spZombieDayAngry = new Sprite(new FileInputStream("resources//ZombieDayAngry.png"), 21, 13);
			spZombieNight = new Sprite(new FileInputStream("resources//ZombieNight.png"), 21, 13);
			spZombieNightAngry = new Sprite(new FileInputStream("resources//ZombieNightAngry.png"), 21, 13);
			spTrap = new Sprite(new FileInputStream("resources//trap.png"), 5, 6);
			spStar = new Sprite(new FileInputStream("resources//star.png"), 1, 1);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
