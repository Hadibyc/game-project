package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class HitAnimation extends Animation {

	private int m_maxIdx;
	private int m_spriteLine = 12;
	private int m_spriteWidth = 13;
	private int m_nbImg = 6;

	public class HitListener implements AnimationListener {
		@Override
		public void done(Animation a) {
			reinitAnimation();
			m_entity.m_isMoving = false;
			m_entity.m_isHitting = false;
		}
	}

	public HitAnimation(Game g, int delay, Entity e) {
		super(g, delay, null);
		m_listener = new HitListener();
		m_entity = e;
		m_sprite = e.m_sprite;
		e.m_game = g;
		changeDirection();
		setPosition(m_entity.m_x, m_entity.m_y, 2);

	}

	@Override
	public void start() {
		m_entity.m_isMoving = true;
		m_entity.m_isHitting = true;
		super.start();
	}

	@Override
	public void setPosition(int x, int y, int scale) {
		m_x = x;
		m_y = y;
		m_scale = scale;
	}

	@Override
	public boolean done() {
		return m_done;
	}

	@Override
	protected boolean nextImage() {
		m_idx++;
		if (m_idx < m_maxIdx) {
			m_img = (BufferedImage) m_sprite.m_images[m_idx];
			return true;
		} else {
			m_done = true;
			m_listener.done(this);
			return false;
		}
	}

	@Override
	protected void paint(Graphics g, int width, int height, int i, int j) {
		g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * i),
				((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * j), m_scale * m_img.getWidth(),
				m_scale * m_img.getHeight(), null);
	}

	public void changeDirection() {
		switch (m_entity.m_direction.toString()) {
		case "N":
			m_idx = m_spriteWidth * m_spriteLine;
			m_maxIdx = m_spriteWidth * m_spriteLine + m_nbImg;
			break;
		case "W":
			m_idx = m_spriteWidth * (m_spriteLine + 1);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 1) + m_nbImg;
			break;
		case "S":
			m_idx = m_spriteWidth * (m_spriteLine + 2);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 2) + m_nbImg;
			break;
		case "E":
			m_idx = m_spriteWidth * (m_spriteLine + 3);
			m_maxIdx = m_spriteWidth * (m_spriteLine + 3) + m_nbImg;
			break;
		}
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
	}

	private void reinitAnimation() {
		m_done = false;
		changeDirection();
	}

}
