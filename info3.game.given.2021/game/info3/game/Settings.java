package info3.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Settings extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		new Settings();
	}

	String[] set = { "Zombie", "J1", "J2", "Decor", "Rabbit" };
	String[] size = new String[60];
	String[] nb_army = new String[100];
	String[] nb_bots = new String[100];
	private JComboBox<String> zombieBox = new JComboBox<String>(set);
	private JComboBox<String> j1Box = new JComboBox<String>(set);
	private JComboBox<String> j2Box = new JComboBox<String>(set);
	private JComboBox<String> decorBox = new JComboBox<String>(set);
	private JComboBox<String> rabbitBox = new JComboBox<String>(set);
	private JComboBox<String> armyBox;
	private JComboBox<String> botsBox;
	private JComboBox<String> sizeBox;
	private JLabel labelZombie = new JLabel();
	private JLabel labelJ1 = new JLabel();
	private JLabel labelJ2 = new JLabel();
	private JLabel labelDecor = new JLabel();
	private JLabel labelRabbit = new JLabel();
	private JLabel labelSize = new JLabel("S");
	private JLabel labelBots = new JLabel("B");
	private JLabel labelArmy = new JLabel("A");
	private JFrame frame;
	private JPanel panel;
	private JButton boutton5;
	private static String[] automatSettings = new String[5];
	private static int[] setting = new int[3];

	public int[] getSettings() {
		return setting;
	}

	public static String[] getAutomatSettings() {
		return automatSettings;
	}

	public Settings() {
		for (int i = 0; i + 40 < 100; i++) {
			size[i] = String.valueOf(i + 40);
		}
		sizeBox = new JComboBox<String>(size);
		for (int i = 0; i < 100; i++) {
			nb_army[i] = String.valueOf(i);
		}
		armyBox = new JComboBox<String>(nb_army);
		for (int i = 0; i < 100; i++) {
			nb_bots[i] = String.valueOf(i);
		}
		botsBox = new JComboBox<String>(nb_bots);
		// setLayout(new FlowLayout());
		frame = new JFrame();
		panel = new JPanel();
		frame.setContentPane(panel);
		panel.setLayout(null);
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		// Box zombie
		zombieBox.setBounds(250, 20, 200, 20);
		panel.add(zombieBox);
		labelZombie.setBounds(50, 20, 200, 20);
		panel.add(labelZombie);
		zombieBox.addActionListener(this);
		zombieBox.setSelectedIndex(0);
		// Box J1
		j1Box.setBounds(250, 60, 200, 20);
		panel.add(j1Box);
		labelJ1.setBounds(50, 60, 200, 20);
		panel.add(labelJ1);
		j1Box.addActionListener(this);
		j1Box.setSelectedIndex(1);
		// Box J2
		j2Box.setBounds(250, 100, 200, 20);
		panel.add(j2Box);
		labelJ2.setBounds(50, 100, 200, 20);
		panel.add(labelJ2);
		j2Box.addActionListener(this);
		j2Box.setSelectedIndex(2);
		// Box Decor
		decorBox.setBounds(250, 140, 200, 20);
		panel.add(decorBox);
		labelDecor.setBounds(50, 140, 200, 20);
		panel.add(labelDecor);
		decorBox.addActionListener(this);
		decorBox.setSelectedIndex(3);
		// Box Rabbit
		rabbitBox.setBounds(250, 180, 200, 20);
		panel.add(rabbitBox);
		labelRabbit.setBounds(50, 180, 200, 20);
		panel.add(labelRabbit);
		rabbitBox.addActionListener(this);
		rabbitBox.setSelectedIndex(4);

		// Box Size
		sizeBox.setBounds(250, 220, 200, 20);
		panel.add(sizeBox);
		labelSize.setBounds(50, 220, 200, 20);
		panel.add(labelSize);
		sizeBox.addActionListener(this);
		sizeBox.setSelectedIndex(0);
		// Box Army
		armyBox.setBounds(250, 260, 200, 20);
		panel.add(armyBox);
		labelArmy.setBounds(50, 260, 200, 20);
		panel.add(labelArmy);
		armyBox.addActionListener(this);
		armyBox.setSelectedIndex(5);
		// Box Bots
		botsBox.setBounds(250, 300, 200, 20);
		panel.add(botsBox);
		labelBots.setBounds(50, 300, 200, 20);
		panel.add(labelBots);
		botsBox.addActionListener(this);
		botsBox.setSelectedIndex(5);

		boutton5 = new JButton("VALIDATE");
		buttcreator(boutton5);
		panel.add(boutton5);
		boutton5.addActionListener(this);
	}

	public static void buttcreator(JButton but) {
		but.setBounds(150, 375, 200, 50);
		but.setFont(new Font("Gill Sans MT", Font.BOLD, 14));
		but.setBackground(new Color(0x2dce98));
		but.setForeground(Color.white);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		e.getSource();
		if (e.getSource().equals(boutton5)) {
			frame.dispose();
		}
		automatSettings[0] = (String) zombieBox.getSelectedItem();
		labelZombie.setText("Zombie : " + automatSettings[0]);

		automatSettings[1] = (String) j1Box.getSelectedItem();
		labelJ1.setText("J1 : " + automatSettings[1]);

		automatSettings[2] = (String) j2Box.getSelectedItem();
		labelJ2.setText("J2 : " + automatSettings[2]);

		automatSettings[3] = (String) decorBox.getSelectedItem();
		labelDecor.setText("Decor : " + automatSettings[3]);

		automatSettings[4] = (String) rabbitBox.getSelectedItem();
		labelRabbit.setText("Rabbit : " + automatSettings[4]);

		setting[0] = Integer.parseInt((String) sizeBox.getSelectedItem());
		labelSize.setText("Size h x w : " + setting[0] + " x " + setting[0]);
		setting[1] = Integer.parseInt((String) armyBox.getSelectedItem());
		labelArmy.setText("Army : " + setting[1]);
		setting[2] = Integer.parseInt((String) botsBox.getSelectedItem());
		labelBots.setText("Bots : " + setting[2]);

	}

}
