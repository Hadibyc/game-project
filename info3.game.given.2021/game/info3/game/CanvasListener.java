/*
 * Copyright (C) 2020  Pr. Olivier Gruber
 * Educational software for a basic game development
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: March, 2020
 *      Author: Pr. Olivier Gruber
 */
package info3.game;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import info3.game.automaton.builder.Aut_Key;
import info3.game.graphics.GameCanvasListener;

public class CanvasListener implements GameCanvasListener {
    Game m_game;
    int m_codeWalkPlayer2;
    int m_code_action;
    String Key_Walk;
    String Key_Action;
    int m_codeWalkPlayer1;
    int m_code_walk;
    Player1 m_p1;
    Player2 m_p2;

    public static final Aut_Key FU = new Aut_Key("FU");
    public static final Aut_Key FD = new Aut_Key("FD");
    public static final Aut_Key FR = new Aut_Key("FR");
    public static final Aut_Key FL = new Aut_Key("FL");
    public static final Aut_Key ENTER = new Aut_Key("ENTER");
    public static final Aut_Key L = new Aut_Key("l");
    public static final Aut_Key M = new Aut_Key("m");
    public static final Aut_Key P = new Aut_Key("p");
    public static final Aut_Key SPACE = new Aut_Key("SPACE");
    public static final Aut_Key Z = new Aut_Key("z");
    public static final Aut_Key Q = new Aut_Key("q");
    public static final Aut_Key S = new Aut_Key("s");
    public static final Aut_Key D = new Aut_Key("d");
    public static final Aut_Key A = new Aut_Key("a");
    public static final Aut_Key E = new Aut_Key("e");
    public static final Aut_Key W = new Aut_Key("w");

    CanvasListener(Game game) {
	m_code_walk = -10;
	m_game = game;
	m_codeWalkPlayer1 = -1;
	m_codeWalkPlayer1 = -2;
    }

    public void getPlayers() {
	m_p1 = m_game.m_model.m_p1;
	m_p2 = m_game.m_model.m_p2;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
//		System.out.println("Mouse clicked: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    public int getM_codeWalkPlayer2() {
	return m_codeWalkPlayer2;
    }

    public void setM_codeWalkPlayer2(int m_codeWalkPlayer2) {
	this.m_codeWalkPlayer2 = m_codeWalkPlayer2;
    }

    public int getM_codeWalkPlayer1() {
	return m_codeWalkPlayer1;
    }

    public void setM_codeWalkPlayer1(int m_codeWalkPlayer1) {
	this.m_codeWalkPlayer1 = m_codeWalkPlayer1;
    }

    @Override
    public void mousePressed(MouseEvent e) {
//		System.out.println("Mouse pressed: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//		System.out.println("Mouse released: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//		System.out.println("Mouse entered: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void mouseExited(MouseEvent e) {
//		System.out.println("Mouse exited: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void mouseDragged(MouseEvent e) {
//		System.out.println("Mouse dragged: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
//		System.out.println("Mouse moved: (" + e.getX() + "," + e.getY() + ")");
//		System.out.println("   modifiers=" + e.getModifiersEx());
//		System.out.println("   buttons=" + e.getButton());
    }

    @Override
    public void keyTyped(KeyEvent e) {
//		System.out.println("Key typed: " + e.getKeyChar() + " code=" + e.getKeyCode());
    }

    @Override
    public void keyPressed(KeyEvent e) {
//	System.out.println("Key pressed: " + e.getKeyChar() + " code=" + e.getKeyCode());
	if (e.getKeyCode() == 10 || e.getKeyCode() == 38 || e.getKeyCode() == 39 || e.getKeyCode() == 40
		|| e.getKeyCode() == 37 || e.getKeyCode() == 76 || e.getKeyCode() == 77 || e.getKeyCode() == 80) {
	    m_codeWalkPlayer1 = e.getKeyCode();
	    switch (e.getKeyCode()) {
	    case 10:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction == null)
			m_p1.setKeyAction(ENTER);
		}
		break;
	    case 37:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk == null)
			m_p1.setKeyWalk(FL);
		}
		break;
	    case 38:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk == null)
			m_p1.setKeyWalk(FU);
		}
		break;
	    case 39:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk == null)
			m_p1.setKeyWalk(FR);
		}
		break;
	    case 40:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk == null)
			m_p1.setKeyWalk(FD);
		}
		break;
	    case 76:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction == null)
			m_p1.setKeyAction(L);
		}
		break;
	    case 77:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction == null)
			m_p1.setKeyAction(M);
		}
		break;
	    case 80:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction == null)
			m_p1.setKeyAction(P);
		}
		break;
	    default:
		break;
	    }
	} else if (e.getKeyCode() == 32 || e.getKeyCode() == 81 || e.getKeyCode() == 83 || e.getKeyCode() == 90
		|| e.getKeyCode() == 68 || e.getKeyCode() == 65 || e.getKeyCode() == 69 || e.getKeyCode() == 87) {
	    m_codeWalkPlayer2 = e.getKeyCode();
	    switch (e.getKeyCode()) {
	    case 32:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction == null)
			m_p2.setKeyAction(SPACE);
		}
		break;
	    case 65:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction == null)
			m_p2.setKeyAction(A);
		}
		break;
	    case 68:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk == null)
			m_p2.setKeyWalk(D);
		}
		break;
	    case 69:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction == null)
			m_p2.setKeyAction(E);
		}
		break;
	    case 81:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk == null)
			m_p2.setKeyWalk(Q);
		}
		break;
	    case 83:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk == null)
			m_p2.setKeyWalk(S);
		}
		break;
	    case 87:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction == null)
			m_p2.setKeyAction(W);
		}
		break;
	    case 90:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk == null)
			m_p2.setKeyWalk(Z);
		}
		break;
	    default:
		break;
	    }
	}
	m_code_walk = e.getKeyCode();
    }

    public void setM_code_action(int m_code_action) {
	this.m_code_action = m_code_action;
    }

    @Override
    public void keyReleased(KeyEvent e) {
//		System.out.println("Key released: " + e.getKeyChar() + " code=" + e.getKeyCode());

	if (e.getKeyCode() == 10 || e.getKeyCode() == 38 || e.getKeyCode() == 39 || e.getKeyCode() == 40
		|| e.getKeyCode() == 37 || e.getKeyCode() == 76 || e.getKeyCode() == 77 || e.getKeyCode() == 80) {
	    m_codeWalkPlayer1 = -1;
	    switch (e.getKeyCode()) {
	    case 10:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction != null && m_p1.m_keyAction.equals(ENTER))
			m_p1.clearKeyAction();
		}
		break;
	    case 37:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk != null && m_p1.m_keyWalk.equals(FL))
			m_p1.clearKeyWalk();
		}
		break;
	    case 38:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk != null && m_p1.m_keyWalk.equals(FU))
			m_p1.clearKeyWalk();
		}
		break;
	    case 39:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk != null && m_p1.m_keyWalk.equals(FR))
			m_p1.clearKeyWalk();
		}
		break;
	    case 40:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyWalk != null && m_p1.m_keyWalk.equals(FD))
			m_p1.clearKeyWalk();
		}
		break;
	    case 76:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction != null && m_p1.m_keyAction.equals(L))
			m_p1.clearKeyAction();
		}
		break;
	    case 77:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction != null && m_p1.m_keyAction.equals(M))
			m_p1.clearKeyAction();
		}
		break;
	    case 80:
		for (Entity m_p1 : m_game.m_model.automatPlayer1) {
		    if (m_p1.m_keyAction != null && m_p1.m_keyAction.equals(P))
			m_p1.clearKeyAction();
		}
		break;
	    default:
		break;
	    }
	} else if (e.getKeyCode() == 32 || e.getKeyCode() == 81 || e.getKeyCode() == 83 || e.getKeyCode() == 90
		|| e.getKeyCode() == 68 || e.getKeyCode() == 65 || e.getKeyCode() == 69 || e.getKeyCode() == 87) {
	    m_codeWalkPlayer2 = -2;
	    switch (e.getKeyCode()) {
	    case 32:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction != null && m_p2.m_keyAction.equals(SPACE))
			m_p2.clearKeyAction();
		}
		break;
	    case 65:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction != null && m_p2.m_keyAction.equals(A))
			m_p2.clearKeyAction();
		}
		break;
	    case 68:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk != null && m_p2.m_keyWalk.equals(D))
			m_p2.clearKeyWalk();
		}
		break;
	    case 69:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction != null && m_p2.m_keyAction.equals(E))
			m_p2.clearKeyAction();
		}
		break;
	    case 81:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk != null && m_p2.m_keyWalk.equals(Q))
			m_p2.clearKeyWalk();
		}
		break;
	    case 83:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk != null && m_p2.m_keyWalk.equals(S))
			m_p2.clearKeyWalk();
		}
		break;
	    case 87:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyAction != null && m_p2.m_keyAction.equals(W))
			m_p2.clearKeyAction();
		}
		break;
	    case 90:
		for (Entity m_p2 : m_game.m_model.automatPlayer2) {
		    if (m_p2.m_keyWalk != null && m_p2.m_keyWalk.equals(Z))
			m_p2.clearKeyWalk();
		}
		break;
	    default:
		break;
	    }
	}
	m_code_walk = -10;
    }

    @Override
    public void tick(long elapsed) {
	m_game.tick(elapsed, m_code_walk, m_codeWalkPlayer1, m_codeWalkPlayer2);
    }

    @Override
    public void paint(Graphics g) {
	m_game.paint(g);
    }

    @Override
    public void windowOpened() {
	m_game.loadMusic();
//    m_game.m_canvas.setTimer(6000);
    }

    @Override
    public void exit() {
    }


    @Override
    public void endOfPlay(String name) {
	m_game.loadMusic();
//    m_expired = false;
    }

    @Override
    public void expired() {
	// will force a change of music, after 6s of play
//    System.out.println("Forcing an ealy change of music");
//    m_expired = true;
//    m_game.loadMusic();    
	}

}
