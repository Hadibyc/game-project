package info3.game;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Images {

	public static BufferedImage grass1;
	public static BufferedImage grass2;
	public static BufferedImage grass3;
	public static BufferedImage grass4;
	public static BufferedImage grass5;
	public static BufferedImage grass6;
	public static BufferedImage grass7;
	public static BufferedImage grass8;
	public static BufferedImage grass9;
	public static BufferedImage grass10;

	public static void loadImages() {
		try {
			grass1 = ImageIO.read(new File("resources/grass1.png"));
			grass2 = ImageIO.read(new File("resources/grass2.png"));
			grass3 = ImageIO.read(new File("resources/grass3.png"));
			grass4 = ImageIO.read(new File("resources/grass4.png"));
			grass5 = ImageIO.read(new File("resources/grass5.png"));
			grass6 = ImageIO.read(new File("resources/grass6.png"));
			grass7 = ImageIO.read(new File("resources/grass7.png"));
			grass8 = ImageIO.read(new File("resources/grass8.png"));
			grass9 = ImageIO.read(new File("resources/grass9.png"));
			grass10 = ImageIO.read(new File("resources/grass10.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
