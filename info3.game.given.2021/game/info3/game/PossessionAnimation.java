package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class PossessionAnimation extends Animation {
	private int m_maxIdx;
	private int m_spriteLine = 0;
	private int m_spriteWidth = 0;
	private int m_nbImg = 4;
	private int cpt = 0;

	public PossessionAnimation(Game g, int delay, Entity e) {
		super(g, delay, null);
		m_game = g;
		m_listener = new PossessionListener();
		m_entity = e;
		m_sprite = e.m_sprite;
		m_idx = 0;
		m_maxIdx = m_spriteWidth * m_spriteLine + m_nbImg;
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
		setPosition(m_entity.m_x, m_entity.m_y, 2);
		super.start();
	}

	public class PossessionListener implements AnimationListener {
		@Override
		public void done(Animation a) {
			m_entity.m_isPossessing = false;
			cpt = 0;
		}
	}

	@Override
	public boolean done() {
		return m_done;
	}

	@Override
	protected boolean nextImage() {
		m_idx++;
		if (m_idx < m_maxIdx) {
			m_img = (BufferedImage) m_sprite.m_images[m_idx];
			return true;
		} else {
			m_done = true;
			m_listener.done(this);
			return false;
		}
	}

	@Override
	protected void paint(Graphics g, int width, int height, int i, int j) {
		if (cpt % 8 < 4)
			g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * i - 5),
					((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * j), m_scale * m_img.getWidth(),
					m_scale * m_img.getHeight(), null);
		else if (cpt % 8 >= 4)
			g.drawImage(m_img, ((width / (2 * (2 * m_entity.m_modelGrid.m_zone + 1))) * i + 5),
					((height / (2 * m_entity.m_modelGrid.m_zone + 1)) * j), m_scale * m_img.getWidth(),
					m_scale * m_img.getHeight(), null);
		cpt++;
	}

	@Override
	public void setPosition(int x, int y, int scale) {
		// TODO Auto-generated method stub
		m_x = x;
		m_y = y;
		m_scale = scale;

	}

}
