package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;

public class ZombieNight extends Zombie {

	public ZombieNight(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.ZOMBIE_DELAY; //
		m_detection = 3;
		m_sprite = SpritesInstances.spZombieNight;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
		m_hitAnimation = new HitAnimation(m_modelGrid.m_game, m_speed, this);
	}

	@Override
	public void setM_detection() {
		if (m_angry) {
			m_detection = 7;
			m_sprite = SpritesInstances.spZombieNightAngry;
		} else {
			m_detection = 5;
			m_sprite = SpritesInstances.spZombieNight;
		}
	}

	@Override
	protected void tick(long elapsed, int codePlayer1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void invertRole() {
		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
		if (m_isMoving)
			m_modelGrid.freePos(old_x, old_y);
		ZombieDay dayForm = new ZombieDay(m_modelGrid, m_automaton, m_category, m_x, m_y);
		dayForm.m_game = m_modelGrid.m_game;
		dayForm.setHp(m_hp);
		dayForm.m_direction = m_direction;
		dayForm.m_movement.changeDirection();
	}

}
