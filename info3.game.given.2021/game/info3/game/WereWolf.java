package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public class WereWolf extends Player2 {
	int m_nb_trap;

	public WereWolf(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_speed = Delays.HUNTER_DELAY;// ajustable
		m_damage = 10; // ajustable
		m_nb_trap = 3;
		m_sprite = SpritesInstances.spWerewolf;
		m_movement = new Movement(m_modelGrid.m_game, m_speed, this, m_sprite);
		m_hitAnimation = new HitAnimation(m_modelGrid.m_game, m_speed, this);
	}

	public void tick(long Elapsed, int code) {
	}

	// La fonction permettant au loup-garou de frapper l'entité devant lui

	public void Hit(Aut_Direction dir) {
		if (!m_isMoving && !m_blocked) {
			m_hitAnimation.start();
			if (Cell(FRONT, A)) {
				Entity toHit = getEntity(FRONT);
				Hurt(this, toHit);
				if (toHit.m_hp <= 0) {
					if (toHit instanceof Player) {
						toHit.m_dieAnimation = new DieAnimation(m_modelGrid.m_game, m_speed, toHit);
						toHit.setM_isDead();
					} else {
						toHit.m_category = V;
						m_game.m_model.freePos(toHit.m_x, toHit.m_y);
						m_game.m_model.deadEntity.add(toHit);
					}
				}

			} else if (Cell(FRONT, V)) {
				return;
			} else {
				Entity toHit = getEntity(FRONT);
				Hurt(this, toHit);
				Hurt(this, this);
				if (this.m_hp <= 0) {
					if (this instanceof Player) {
						this.m_dieAnimation = new DieAnimation(m_modelGrid.m_game, m_speed, this);
						this.setM_isDead();
					} else {
						this.m_category = V;
						m_game.m_model.freePos(this.m_x, this.m_y);
						m_game.m_model.deadEntity.add(this);
					}
				}
				if (toHit.m_hp <= 0) {
					toHit.m_category = V;
					m_game.m_model.freePos(toHit.m_x, toHit.m_y);
					m_game.m_model.deadEntity.add(toHit);

				}
			}
		}
	}

	@Override
	public void Wizz(Aut_Direction d) {
		Entity e = getEntity(FRONT);
		if (Cell(m_direction, D)) {
			m_nb_trap++;
			m_modelGrid.freePos(e.m_x, e.m_y);
		} else if (Cell(m_direction, V) && m_nb_trap > 0) {
			m_nb_trap--;
			m_modelGrid.m_grid[e.m_y][e.m_x] = new Trap(m_modelGrid, m_modelGrid.m_autTrap, D, e.m_x, e.m_y);
		}

	}

	@Override
	public void invertRole() {
		m_modelGrid.deadEntity.add(this);
		m_modelGrid.freePos(m_x, m_y);
		if (m_isMoving) {
			m_modelGrid.freePos(old_x, old_y);
			m_modelGrid.freePos(m_x, m_y);

		}

		this.m_blocked = true;

		Hider nightForm = new Hider(m_modelGrid, m_automaton, A, m_x, m_y);
		nightForm.m_game = m_modelGrid.m_game;
		nightForm.setHp(m_hp);
		nightForm.m_direction = m_direction;
		nightForm.m_movement.changeDirection();
		nightForm.m_keyWalk = this.m_keyWalk;
		nightForm.m_keyAction = this.m_keyAction;
		m_modelGrid.m_hider = nightForm;
		m_modelGrid.m_p2 = nightForm;
		if (m_modelGrid.automatedSettings[2].equals("J1")) {
			m_modelGrid.automatPlayer1.remove(this);
			m_modelGrid.automatPlayer1.add(nightForm);
		} else if (m_modelGrid.automatedSettings[2].equals("J2")) {
			m_modelGrid.automatPlayer2.remove(this);
			m_modelGrid.automatPlayer2.add(nightForm);
		}
	}
}
