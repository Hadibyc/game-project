package info3.game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public abstract class Animation {

	Sprite m_sprite;
	int m_idx;
	int m_scale;
	BufferedImage m_img;
	boolean m_done;
	int m_x, m_y;
	Entity m_entity;

	public interface AnimationListener {
		void done(Animation a);
	}

	AnimationListener m_listener;
	int m_delay;
	Game m_game;

	public Animation(Game g, int delay, AnimationListener l) {
		m_game = g;
		m_listener = l;
		m_delay = delay;
	}

	public void start() {
		new _AnimationListener();
	}

	/**
	 * Sets the position and scale of the animation
	 * 
	 * @param x
	 * @param y
	 * @param scale
	 */
	public abstract void setPosition(int x, int y, int scale);

	/**
	 * @return true if the animation is done.
	 */
	public abstract boolean done();

	/**
	 * Moves onto the next image.
	 * 
	 * @return true is the animation has more images.
	 */
	protected abstract boolean nextImage();

	/*
	 * Paints the current image at the given position and scale.
	 */
	protected abstract void paint(Graphics g, int width, int height, int i, int j);

	public void updateSprite(Entity e) {
		m_sprite = e.m_sprite;
		m_img = (BufferedImage) m_sprite.m_images[m_idx];
	}

	private class _AnimationListener implements TimerListener {

		_AnimationListener() {
			Timers t = m_game.getTimers();
			t.setTimer(m_delay, this);
		}

		@Override
		public void expired() {
			if (nextImage()) {
				Timers t = m_game.getTimers();
				t.setTimer(m_delay, this);
			} else {
				if (m_listener != null) {
					m_listener.done(Animation.this);
				}

			}
		}

	}
}
