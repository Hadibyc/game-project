package info3.game;

import info3.game.automaton.builder.Aut_Automaton;
import info3.game.automaton.builder.Aut_Category;
import info3.game.automaton.builder.Aut_Direction;

public abstract class Zombie extends Entity {

	public static int X_HIDER;
	public static int Y_HIDER;
	protected int m_detection;
	protected boolean m_angry;

	public int getM_detection() {
		return m_detection;
	}

	public abstract void setM_detection();

	public int getM_speed() {
		return m_speed;
	}

	public int getM_damage() {
		return m_damage;
	}

	public boolean getM_angry() {
		return m_angry;
	}

	Zombie(ModelGrid model, Aut_Automaton a, Aut_Category c, int x, int y) {
		super(model, a, c, x, y);
		m_hp = 1000; //
		m_damage = 10; //
		m_angry = false;
	}

	public int distance(Entity e1, int x, int y) {
		return Math.abs((int) Math.sqrt((double) Math.pow(x - e1.getX(), 2.0) + Math.pow(y - e1.getY(), 2.0)));
	}

	@Override
	public boolean Closest(Aut_Category c, Aut_Direction d) {

		int closest_x = X_HIDER;
		int closest_y = Y_HIDER;
		int d_min = distance(this, X_HIDER, Y_HIDER);
		Aut_Direction dir;

		// calcul du point le plus proche

		int dist = distance(this, X_HIDER - ModelGrid.WIDTH, Y_HIDER);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER - ModelGrid.WIDTH;
			closest_y = Y_HIDER;
		}
		dist = distance(this, X_HIDER, Y_HIDER - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER;
			closest_y = Y_HIDER - ModelGrid.HEIGHT;
		}
		dist = distance(this, X_HIDER - ModelGrid.WIDTH, Y_HIDER - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER - ModelGrid.WIDTH;
			closest_y = Y_HIDER - ModelGrid.HEIGHT;
		}
		dist = distance(this, X_HIDER + ModelGrid.WIDTH, Y_HIDER);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER + ModelGrid.WIDTH;
			closest_y = Y_HIDER;
		}

		dist = distance(this, X_HIDER, Y_HIDER + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER;
			closest_y = Y_HIDER + ModelGrid.HEIGHT;
		}
		dist = distance(this, X_HIDER + ModelGrid.WIDTH, Y_HIDER - ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER + ModelGrid.WIDTH;
			closest_y = Y_HIDER - ModelGrid.HEIGHT;
		}
		dist = distance(this, X_HIDER - ModelGrid.WIDTH, Y_HIDER + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER - ModelGrid.WIDTH;
			closest_y = Y_HIDER + ModelGrid.HEIGHT;
		}
		dist = distance(this, X_HIDER + ModelGrid.WIDTH, Y_HIDER + ModelGrid.HEIGHT);
		if (dist < d_min) {
			d_min = dist;
			closest_x = X_HIDER + ModelGrid.WIDTH;
			closest_y = Y_HIDER + ModelGrid.HEIGHT;
		}

		// test si il est dans le rayon de detection

		if (d_min > m_detection) {
			m_angry = false;
			setM_detection();
			return false;
		}

		// calcul de la direction à laquelle il est le plus proche de this

		int d_min1;
		dist = distance(this, closest_x + m_detection, closest_y);
		d_min1 = dist;
		dir = WEST;

		dist = distance(this, closest_x - m_detection, closest_y);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = EAST;
		}
		dist = distance(this, closest_x, closest_y + m_detection);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = NORTH;
		}
		dist = distance(this, closest_x, closest_y - m_detection);
		if (dist < d_min1) {
			d_min1 = dist;
			dir = SOUTH;
		}

		// test si c'est la bonne direction

		if (dir.equals(d)) {
			m_angry = true;
			setM_detection();
			return true;
		}

		return false;
	}

	@Override
	public void Pop(Aut_Direction dir) {
		Hit(dir);
	}

	@Override
	public void Wizz(Aut_Direction dir) {
		Wait();
	}
}
